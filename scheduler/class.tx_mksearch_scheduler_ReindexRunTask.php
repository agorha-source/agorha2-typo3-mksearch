<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2020 Sword SSL
 *  All rights reserved
 ***************************************************************/
use TYPO3\CMS\Core\Utility\GeneralUtility;

class tx_mksearch_scheduler_ReindexRunTask extends tx_mksearch_scheduler_ReindexAbstractTask
{
    /**
     * Function executed from the Scheduler.
     */
    public function execute()
    {
        $success = true;

//        if (!$this->areMultipleExecutionsAllowed()) {
//            $this->getExecution()->setMultiple(true);
//            $this->save();
//        }

        try {
            $rows = tx_mksearch_util_ServiceRegistry::getIntReindexService()->triggerQueueIndexing($this->getAmountOfItems());
            if (!empty($rows)) {//sinon il y a un avertissement PHP à array_merge
                $rows = count(call_user_func_array('array_merge', array_values($rows)));
            }
            $msg = sprintf($rows ? '%d item(s) indexed' : 'No items in reindex queue.', $rows);
            if ($rows) {
                tx_rnbase_util_Logger::info($msg, 'mksearch');
            }
        } catch (Exception $e) {
            tx_rnbase_util_Logger::fatal("L'exécution de la réindexation a échoué!", 'mksearch', ['Exception' => $e->getMessage()]);
            $success = false;
        }

        return $success;
    }

    /**
     * @return string Information to display
     */
    public function getAdditionalInformation()
    {
        return sprintf(
            "Enregistrements dans la file d'attente, par types de contenu: %s
            Réindexe %d éléments par exécution. Actuellement %d éléments dans la file d'attente.",
            $this->countAllIndexableRecordsByType(),
            $this->getAmountOfItems(),
            $this->getItemsInQueue()
        );
    }

    /**
     * @return string
     */
    private function countAllIndexableRecordsByType(){
        $counts = [];
        $indexers = tx_mksearch_util_Config::getIndexers();
        foreach($indexers as $indexer){
            $className = get_class($indexer);
            $tableName = $this->getTableNameByIndexer($indexer);
            $counts[$className] = $this->queueCountByTableName($tableName);
        }
        $return = "";
        $total = 0;
        foreach($counts as $type => $val){
            $return .= "\r\n- {$type} ({$val})";
            $total += $val;
        }
        return $return . "\r\n>>> TOTAL ({$total})";
    }

    /**
     * @param string $tablename
     * @return array
     */
    protected function queueCountByTableName($tablename){
        $queryBuilder = GeneralUtility::makeInstance(TYPO3\CMS\Core\Database\ConnectionPool::class)
            ->getQueryBuilderForTable(self::QUEUE_TABLE);
        $statement = $queryBuilder
            ->count('uid')
            ->from(self::QUEUE_TABLE)
            ->where(
                $queryBuilder->expr()->eq('tablename', $queryBuilder->createNamedParameter($tablename, \PDO::PARAM_STR)),
                $queryBuilder->expr()->eq('deleted', $queryBuilder->createNamedParameter(0, \PDO::PARAM_INT)),
                $queryBuilder->expr()->eq('being_indexed', $queryBuilder->createNamedParameter(0, \PDO::PARAM_INT))
            )
            ->execute();
        return $statement->fetchColumn(0);
    }

}

if (defined('TYPO3_MODE') && $GLOBALS['TYPO3_CONF_VARS'][TYPO3_MODE]['XCLASS']['ext/mksearch/scheduler/class.tx_mksearch_scheduler_ReindexRunTask.php']) {
    include_once $GLOBALS['TYPO3_CONF_VARS'][TYPO3_MODE]['XCLASS']['ext/mksearch/scheduler/class.tx_mksearch_scheduler_ReindexRunTask.php'];
}
