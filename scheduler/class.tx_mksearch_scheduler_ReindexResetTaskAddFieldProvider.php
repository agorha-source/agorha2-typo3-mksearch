<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2020 Sword SSL
 *  All rights reserved
 ***************************************************************/

define('FIELD_QUEUE', 'selectedQueue');
define('FIELD_FORCE_DELETE', 'forceDelete');

class tx_mksearch_scheduler_ReindexResetTaskAddFieldProvider extends Tx_Rnbase_Scheduler_FieldProvider {
    /**
     * This method is used to define new fields for adding or editing a task
     * In this case, it adds an email field.
     *
     * @param array $taskInfo :        reference to the array containing the info used in the add/edit form
     * @param Tx_Rnbase_Scheduler_Task $task :            when editing, reference to the current task object. Null when adding.
     * @param \TYPO3\CMS\Scheduler\Controller\SchedulerModuleController $schedulerModule : reference to the calling object (Scheduler's BE module)
     *
     * @return array Array containg all the information pertaining to the additional fields
     *               The array is multidimensional, keyed to the task class name and each field's id
     *               For each field it provides an associative sub-array with the following:
     *               ['code']        => The HTML code for the field
     *               ['label']       => The label of the field (possibly localized)
     *               ['cshKey']      => The CSH key for the field
     *               ['cshLabel']    => The code of the CSH label
     */
    protected function _getAdditionalFields(array &$taskInfo, $task, $schedulerModule)
    {
        $action =  $schedulerModule->getCurrentAction();
        $additionalFields = [];

        if (!array_key_exists(FIELD_QUEUE, $taskInfo) || empty($taskInfo[FIELD_QUEUE])) {
            if ('add' == $action) {
                // New task
                $taskInfo[FIELD_QUEUE] = [];
            } elseif ('edit' == $action) {
                // Editing a task, set to internal value if data was not submitted already
                $taskInfo[FIELD_QUEUE] = $task->getQueue();
            } else {
                // Otherwise set an empty value, as it will not be used anyway
                $taskInfo[FIELD_QUEUE] = [];
            }
        }
        // Write the code for the field
        $fieldID = 'field_'.FIELD_QUEUE;
        // Note: Name qualifier MUST be "tx_scheduler" as the tx_scheduler's BE module is used!
        $fieldCode = "<select name='tx_scheduler[" . FIELD_QUEUE . "]' id='{$fieldID}' class='form-control'>";
        $queues = [
            'tx_mksearch_queue_reindex' => "Réindexation",
            'tx_mksearch_queue' => "Indexation",
        ];
        foreach($queues as $key => $val){
            $selected = ($key === $taskInfo[FIELD_QUEUE])?'selected':'';
            $fieldCode .= "<option value='{$key}' {$selected}>{$val}</option>";
        }

        $fieldCode .= "</select>";
        $additionalFields[$fieldID] = [
            'code' => $fieldCode,
            'label' => "File d'attente à vider",
            'cshKey' => '_MOD_web_txschedulerM1',
        ];

        if (!array_key_exists(FIELD_FORCE_DELETE, $taskInfo) || empty($taskInfo[FIELD_FORCE_DELETE])) {
            if ('add' == $action) {
                // New task
                $taskInfo[FIELD_FORCE_DELETE] = '';
            } elseif ('edit' == $action) {
                // Editing a task, set to internal value if data was not submitted already
                $taskInfo[FIELD_FORCE_DELETE] = $task->getForceDelete();
            } else {
                // Otherwise set an empty value, as it will not be used anyway
                $taskInfo[FIELD_FORCE_DELETE] = '';
            }
        }
        // Write the code for the field
        $fieldID = 'field_'.FIELD_FORCE_DELETE;
        // Note: Name qualifier MUST be "tx_scheduler" as the tx_scheduler's BE module is used!
        $checked = ($taskInfo[FIELD_FORCE_DELETE])?"checked='checked'":'';
        $fieldCode = "<input type='checkbox' name='tx_scheduler[".FIELD_FORCE_DELETE."]' id='{$fieldID}' {$checked}/>";
        $additionalFields[$fieldID] = [
            'code' => $fieldCode,
            'label' => "Forcer la suppression définitive de tous les éléments de la file d'attente (inclut les éléments précédemment désactivés)",
            'cshKey' => '_MOD_web_txschedulerM1',
        ];
        
        return $additionalFields;
    }

    /**
     * This method checks any additional data that is relevant to the specific task
     * If the task class is not relevant, the method is expected to return true.
     *
     * @param array $submittedData :   reference to the array containing the data submitted by the user
     * @param \TYPO3\CMS\Scheduler\Controller\SchedulerModuleController $schedulerModule : reference to the calling object (Scheduler's BE module)
     *
     * @return bool True if validation was ok (or selected class is not relevant), false otherwise
     */
    protected function _validateAdditionalFields(array &$submittedData, $schedulerModule)
    {
        return true;
    }

    /**
     * This method is used to save any additional input into the current task object
     * if the task class matches.
     *
     * @param array $submittedData : array containing the data submitted by the user
     * @param tx_mksearch_scheduler_ReindexRunTask $task :          reference to the current task object
     * @throws Exception
     */
    protected function _saveAdditionalFields(array $submittedData, Tx_Rnbase_Scheduler_Task $task)
    {
        $task->setQueue($submittedData[FIELD_QUEUE]);
        $task->setForceDelete($submittedData[FIELD_FORCE_DELETE]);
    }
}

if (defined('TYPO3_MODE') && $GLOBALS['TYPO3_CONF_VARS'][TYPO3_MODE]['XCLASS']['ext/mksearch/scheduler/class.tx_mksearch_scheduler_ReindexResetTaskAddFieldProvider.php']) {
    include_once $GLOBALS['TYPO3_CONF_VARS'][TYPO3_MODE]['XCLASS']['ext/mksearch/scheduler/class.tx_mksearch_scheduler_ReindexResetTaskAddFieldProvider.php'];
}
