<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2020 Sword SSl
*  All rights reserved
*
***************************************************************/

define('FIELD_ITEMS', 'amountOfItems');
define('FIELD_CONTENT_TYPES', 'contentTypes');

/**
 * Class tx_mksearch_scheduler_ReindexFeedTaskAddFieldProvider
 */
class tx_mksearch_scheduler_ReindexFeedTaskAddFieldProvider extends Tx_Rnbase_Scheduler_FieldProvider
{
    protected function _getAdditionalFields(array &$taskInfo, $task, $schedulerModule)
    {
        $action =  $schedulerModule->getCurrentAction();
        $additionalFields = [];

        if (!array_key_exists(FIELD_ITEMS, $taskInfo) || empty($taskInfo[FIELD_ITEMS])) {
            if ('add' == $action) {
                // New task
                $taskInfo[FIELD_ITEMS] = '';
            } elseif ('edit' == $action) {
                // Editing a task, set to internal value if data was not submitted already
                $taskInfo[FIELD_ITEMS] = $task->getAmountOfItems();
            } else {
                // Otherwise set an empty value, as it will not be used anyway
                $taskInfo[FIELD_ITEMS] = '';
            }
        }
        // Write the code for the field
        $fieldID = 'field_'.FIELD_ITEMS;
        // Note: Name qualifier MUST be "tx_scheduler" as the tx_scheduler's BE module is used!
        $fieldCode = '<input type="text" name="tx_scheduler['.FIELD_ITEMS.']" id="'.$fieldID.
            '" value="'.$taskInfo[FIELD_ITEMS].'" size="10" />';
        $additionalFields[$fieldID] = [
            'code' => $fieldCode,
            'label' => 'LLL:EXT:mksearch/locallang_db.xml:scheduler_indexTask_field_'.FIELD_ITEMS,
            'cshKey' => '_MOD_web_txschedulerM1',
        ];

        if (!array_key_exists(FIELD_CONTENT_TYPES, $taskInfo) || empty($taskInfo[FIELD_CONTENT_TYPES])) {
            if ('add' == $action) {
                // New task
                $taskInfo[FIELD_CONTENT_TYPES] = [];
            } elseif ('edit' == $action) {
                // Editing a task, set to internal value if data was not submitted already
                $taskInfo[FIELD_CONTENT_TYPES] = $task->getContentTypes();
            } else {
                // Otherwise set an empty value, as it will not be used anyway
                $taskInfo[FIELD_CONTENT_TYPES] = [];
            }
        }
        // Write the code for the field
        $fieldID = 'field_'.FIELD_CONTENT_TYPES;
        // Note: Name qualifier MUST be "tx_scheduler" as the tx_scheduler's BE module is used!
        $fieldCode = "<select multiple name='tx_scheduler[" . FIELD_CONTENT_TYPES . "][]' id='{$fieldID}' size='10' class='form-control'>";
        $indexers = tx_mksearch_util_Config::getIndexers();
        foreach($indexers as $indexer){
            $contentType = implode('.',$indexer->getContentType());
            $selected = (array_search($contentType, $taskInfo[FIELD_CONTENT_TYPES]) !== false)?'selected':'';
            $fieldCode .= "<option value='{$contentType}' {$selected}>{$contentType}</option>";
        }
        $fieldCode .= "</select>";

        $additionalFields[$fieldID] = [
            'code' => $fieldCode,
            'label' => "Contenus à indexer",
            'cshKey' => '_MOD_web_txschedulerM1',
        ];

        return $additionalFields;
    }

    /**
     * This method checks any additional data that is relevant to the specific task
     * If the task class is not relevant, the method is expected to return true.
     *
     * @param array                                                     $submittedData:   reference to the array containing the data submitted by the user
     * @param \TYPO3\CMS\Scheduler\Controller\SchedulerModuleController $schedulerModule: reference to the calling object (Scheduler's BE module)
     *
     * @return bool True if validation was ok (or selected class is not relevant), false otherwise
     */
    protected function _validateAdditionalFields(array &$submittedData, $schedulerModule)
    {
        if(!is_numeric($submittedData[FIELD_ITEMS])){
            return false;
        }
        if(!is_array($submittedData[FIELD_CONTENT_TYPES])){
            return false;
        }
        return true;
    }

    /**
     * This method is used to save any additional input into the current task object
     * if the task class matches.
     *
     * @param array $submittedData : array containing the data submitted by the user
     * @param tx_mksearch_scheduler_ReindexFeedTask $task :          reference to the current task object
     * @throws Exception
     */
    protected function _saveAdditionalFields(array $submittedData, Tx_Rnbase_Scheduler_Task $task)
    {
        $task->setAmountOfItems($submittedData[FIELD_ITEMS]);
        $task->setContentTypes($submittedData[FIELD_CONTENT_TYPES]);
    }
}

if (defined('TYPO3_MODE') && $GLOBALS['TYPO3_CONF_VARS'][TYPO3_MODE]['XCLASS']['ext/mksearch/scheduler/class.tx_mksearch_scheduler_ReindexFeedTaskAddFieldProvider.php']) {
    include_once $GLOBALS['TYPO3_CONF_VARS'][TYPO3_MODE]['XCLASS']['ext/mksearch/scheduler/class.tx_mksearch_scheduler_ReindexFeedTaskAddFieldProvider.php'];
}
