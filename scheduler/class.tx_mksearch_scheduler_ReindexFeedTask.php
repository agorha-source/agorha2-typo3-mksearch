<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2020 Sword SSL
 *  All rights reserved
 ***************************************************************/

use TYPO3\CMS\Core\Utility\GeneralUtility;

class tx_mksearch_scheduler_ReindexFeedTask extends tx_mksearch_scheduler_ReindexAbstractTask
{
    /**
     * Content types to be indexed at one run.
     *
     * @var array
     */
    protected $contentTypesToIndexPerRun;

    /**
     * Function executed from the Scheduler.
     */
    public function execute()
    {
        $success = true;

//        if (!$this->areMultipleExecutionsAllowed()) {
//            $this->getExecution()->setMultiple(true);
//            $this->save();
//        }

        try {
            $this->indexSelectedContentTypes();
        } catch (Exception $e) {
            tx_rnbase_util_Logger::fatal("La génération de la file d'attente pour la réindexation complète a échoué!", 'mksearch', ['Exception' => $e->getMessage()]);
            $success = false;
        }

        return $success;
    }

    /**
     * @return string Information to display
     */
    public function getAdditionalInformation()
    {
        return sprintf(
            "Enregistrements indexables dans la bdd, par types de contenu sélectionnés: %s
            Ajoute %d éléments par exécution. Actuellement %d éléments dans la file d'attente.",
            $this->countSelectedContentTypes(),
            $this->getAmountOfItems(),
            $this->getItemsInQueue()
        );
    }

    /**
     * @return void
     */
    private function indexSelectedContentTypes(){
        //$indexers = tx_mksearch_util_Config::getIndexers();
        $selectedContentTypes = $this->getContentTypes();
        foreach($selectedContentTypes as $type){
            $indexer = $this->getIndexerByContentType($type);
            $tableName = $this->getTableNameByIndexer($indexer);
            $records = $this->getIndexableRecords($tableName);
            $this->addRecordsToIndex($tableName, $records);
        }
    }

    /**
     * @param $tableName
     * @param $records
     */
    private function addRecordsToIndex($tableName, $records){
        foreach($records as $record){
            if(!$this->isRecordInQueue($tableName, $record)){
                tx_mksearch_util_ServiceRegistry::getIntReindexService()->addRecordToIndex($tableName, $record['uid']);
            }
        }
    }

    /**
     * @param $tableName
     * @param $record
     * @return bool
     */
    private function isRecordInQueue($tableName, $record){
        // check existence only for core indexers
        if(      $tableName !== 'pages'
            xor  $tableName !== 'tt_content'
            xor  $tableName !== 'sys_file'
        ){
            return false;
        }
        // check if record already in queue
        $queryBuilder = GeneralUtility::makeInstance(\TYPO3\CMS\Core\Database\ConnectionPool::class)
            ->getQueryBuilderForTable(self::QUEUE_TABLE);
//        $queryBuilder
//            ->getRestrictions()
//            ->removeAll();
        $statement = $queryBuilder
            ->count('*')
            ->from(self::QUEUE_TABLE)
            ->where(
                $queryBuilder->expr()->eq('recid', $queryBuilder->createNamedParameter($record['uid'], \PDO::PARAM_INT)),
                $queryBuilder->expr()->eq('tablename', $queryBuilder->createNamedParameter($tableName, \PDO::PARAM_STR)),
                $queryBuilder->expr()->eq('deleted', $queryBuilder->createNamedParameter(0, \PDO::PARAM_INT)),
                $queryBuilder->expr()->eq('being_indexed', $queryBuilder->createNamedParameter(0, \PDO::PARAM_INT))
            );
        if($statement->execute()->fetchColumn(0) > 0){
            return true;
        }
        return false;
    }

    /**
     * @param $tableName
     * @return mixed[array|int]
     */
    private function getIndexableRecords($tableName, $count = false){
        $queryBuilder = GeneralUtility::makeInstance(\TYPO3\CMS\Core\Database\ConnectionPool::class)
            ->getQueryBuilderForTable($tableName);
        $queryBuilder->getRestrictions()->removeAll();
        if($count){
            $queryBuilder->count('uid');
        }else{
            $queryBuilder->select('uid');
        }
        $queryBuilder->from($tableName);

        if($tableName === 'pages'){         // pages standard uniquement
            $queryBuilder->where(
                $queryBuilder->expr()->eq('doktype', $queryBuilder->createNamedParameter(1, \PDO::PARAM_INT))
            );
        }

        if($tableName === 'tt_content'){    // only configured CTypes
            $cTypes = $this->getCTypesConfig($tableName);
            foreach($cTypes as $cType){
                $cType = trim($cType);
                $queryBuilder->orWhere(
                    $queryBuilder->expr()->eq('CType', $queryBuilder->createNamedParameter($cType, \PDO::PARAM_STR))
                );
            }
        }

        if($count){
            return $queryBuilder->execute()->fetchColumn(0);
        }else{
            return $queryBuilder->execute()->fetchAll();
        }
    }

    /**
     * Return content types.
     *
     * @return array
     */
    public function getContentTypes()
    {
        return $this->contentTypesToIndexPerRun;
    }

    /**
     * Set content types.
     *
     * @param array $val
     * @throws Exception
     */
    public function setContentTypes($val)
    {
        if ( !is_array($val) ) {
            throw new Exception('tx_mksearch_scheduler_TaskTriggerIndexingQueue->setContentTypes(): Invalid content types given!');
        }
        // else
        $this->contentTypesToIndexPerRun = $val;
    }

    /**
     * @param $tableName
     * @return array
     *
     * @todo cache result for approximated task execution time
     */
    private function getCTypesConfig($tableName): array
    {
        $indexer = tx_mksearch_util_Config::getIndexersForDatabaseTables([$tableName]);
        $config = tx_mksearch_util_Config::getIndexerDefaultTSConfig($indexer[0]['extKey'], $indexer[0]['contentType']);
        $search_start = strpos($config, 'includeCTypesInGridelementRendering');
        $search_end = strpos($config, "\r\n", $search_start);
        $lineLength = $search_end - $search_start;
        $cTypeConfig = substr($config, $search_start, $lineLength);
        $cTypeConfig = explode(' = ', $cTypeConfig);
        return explode(',', $cTypeConfig[1]);
    }

    /**
     * Return a text with numbers of records available for reindexing of selected content types.
     *
     * @return string
     */
    protected function countSelectedContentTypes()
    {
        $return = "";
        $total = 0;
        foreach($this->getContentTypes() as $type){
            $indexer = $this->getIndexerByContentType($type);
            $className = get_class($indexer);
            $tableName = $this->getTableNameByIndexer($indexer);
            $count = $this->getIndexableRecords($tableName, true);
            $return .= "\r\n - {$className} ({$count})";
            $total += $count;
        };
        return $return . "\r\n>>> TOTAL ({$total})";
    }

    /**
     * @param $type
     * @return tx_mksearch_interface_Indexer
     */
    protected function getIndexerByContentType($type): tx_mksearch_interface_Indexer
    {
        list($extKey, $contentType) = explode('.', $type);
        $indexer = tx_mksearch_util_Config::getIndexerByType($extKey, $contentType);
        return $indexer;
    }
}

if (defined('TYPO3_MODE') && $GLOBALS['TYPO3_CONF_VARS'][TYPO3_MODE]['XCLASS']['ext/mksearch/scheduler/class.tx_mksearch_scheduler_ReindexFeedTask.php']) {
    include_once $GLOBALS['TYPO3_CONF_VARS'][TYPO3_MODE]['XCLASS']['ext/mksearch/scheduler/class.tx_mksearch_scheduler_ReindexFeedTask.php'];
}
