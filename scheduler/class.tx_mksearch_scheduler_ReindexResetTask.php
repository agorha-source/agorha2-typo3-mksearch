<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2020 Sword SSL
 *  All rights reserved
 ***************************************************************/
use TYPO3\CMS\Core\Utility\GeneralUtility;

class tx_mksearch_scheduler_ReindexResetTask extends tx_mksearch_scheduler_ReindexAbstractTask
{
    /**
     * Selected queue (table name).
     *
     * @var string
     */
    protected $queue;
    /**
     * Force delete query instead of soft delete.
     *
     * @var bool
     */
    protected $forceDelete;

    /**
     * Function executed from the Scheduler.
     */
    public function execute()
    {
        $success = true;

//        if (!$this->areMultipleExecutionsAllowed()) {
//            $this->getExecution()->setMultiple(true);
//            $this->save();
//        }

        try {
            if($this->getForceDelete()){
                $this->emptyTheQueue();
            }else{
                $this->resetTheQueue();
            }
            if (!empty($rows)) {    //sinon il y a un avertissement PHP à array_merge
                $rows = count(call_user_func_array('array_merge', array_values($rows)));
            }
            $msg = sprintf($rows ? '%d item(s) indexed' : 'No items in reindex queue.', $rows);
            if ($rows) {
                tx_rnbase_util_Logger::info($msg, 'mksearch');
            }
        } catch (Exception $e) {
            tx_rnbase_util_Logger::fatal('Run reindexing failed!', 'mksearch', ['Exception' => $e->getMessage()]);
            $success = false;
        }

        return $success;
    }

    /**
     * @return string Information to display
     */
    public function getAdditionalInformation()
    {
        $info = "File d'attente sélectionnée: ";

        if ($this->getQueue() == 'tx_mksearch_queue') {
            $info .= "Indexation";
        }
        else { //default
            $info .= "Réindexation";
        }

        $info .= "\r\n Suppression définitive: ";

        if( $this->getForceDelete() ){
            $info .= "Oui";
        }
        else { //default
            $info .= "Non (soft delete)";
        }

        $info .= "\r\n Éléments supprimables: ";
        $info .= $this->countIndexableRecordsByQueue();

        return $info;
    }

    /**
     * @return \Doctrine\DBAL\Driver\Statement|int
     */
    private function resetTheQueue(){
        $queryBuilder = GeneralUtility::makeInstance(TYPO3\CMS\Core\Database\ConnectionPool::class)
            ->getQueryBuilderForTable($this->getQueue());
        return $queryBuilder
            ->update($this->getQueue())
            ->where(
                $queryBuilder->expr()->eq('deleted', $queryBuilder->createNamedParameter(0))
            )
            ->set('deleted', 1)
            ->execute();
    }

    /**
     * @return \Doctrine\DBAL\Driver\Statement|int
     */
    private function emptyTheQueue(){
        $queryBuilder = GeneralUtility::makeInstance(TYPO3\CMS\Core\Database\ConnectionPool::class)
            ->getQueryBuilderForTable($this->getQueue());
        return $queryBuilder
            ->delete($this->getQueue())
            ->where(
                $queryBuilder->expr()->neq('uid', $queryBuilder->createNamedParameter(0))
            )
            ->execute();
    }

    /**
     * Return a text with numbers of records available for reindexing of selected content types.
     *
     * @return string
     */
    private function countIndexableRecordsByQueue()
    {
        $queryBuilder = GeneralUtility::makeInstance(TYPO3\CMS\Core\Database\ConnectionPool::class)
            ->getQueryBuilderForTable($this->getQueue());
        $queryBuilder
            ->count('uid')
            ->from($this->getQueue());
        if(! $this->getForceDelete() ) {
            $queryBuilder
                ->where(
                    $queryBuilder->expr()->eq('deleted', $queryBuilder->createNamedParameter(0, \PDO::PARAM_INT)),
                    $queryBuilder->expr()->eq('being_indexed', $queryBuilder->createNamedParameter(0, \PDO::PARAM_INT))
                );
        }
        return $queryBuilder->execute()->fetchColumn(0);
    }

    /**
     * Return amount of items.
     *
     * @return string
     */
    public function getQueue()
    {
        return $this->queue;
    }

    /**
     * Set amount of items.
     *
     * @param string $val
     * @throws Exception
     */
    public function setQueue($val)
    {
        $val = strval($val);
        $this->queue = $val;
    }

    /**
     * Return amount of items.
     *
     * @return bool
     */
    public function getForceDelete()
    {
        return $this->forceDelete;
    }

    /**
     * Set amount of items.
     *
     * @param bool $val
     * @throws Exception
     */
    public function setForceDelete($val)
    {
        $val = boolval($val);
        $this->forceDelete = $val;
    }
}

if (defined('TYPO3_MODE') && $GLOBALS['TYPO3_CONF_VARS'][TYPO3_MODE]['XCLASS']['ext/mksearch/scheduler/class.tx_mksearch_scheduler_ReindexResetTask.php']) {
    include_once $GLOBALS['TYPO3_CONF_VARS'][TYPO3_MODE]['XCLASS']['ext/mksearch/scheduler/class.tx_mksearch_scheduler_ReindexResetTask.php'];
}
