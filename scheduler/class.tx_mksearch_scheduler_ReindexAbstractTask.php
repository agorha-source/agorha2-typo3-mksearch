<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2020 Sword SSL
*  All rights reserved
*
***************************************************************/

abstract class tx_mksearch_scheduler_ReindexAbstractTask extends Tx_Rnbase_Scheduler_Task
{
    const QUEUE_TABLE = 'tx_mksearch_queue_reindex';

    /**
     * Amount of items to be indexed at one run.
     *
     * @var int
     */
    protected $amountOfItemsToIndexPerRun;

    /**
     * Return amount of items.
     *
     * @return int
     */
    public function getAmountOfItems()
    {
        return $this->amountOfItemsToIndexPerRun;
    }

    /**
     * Set amount of items.
     *
     * @param int $val
     * @throws Exception
     */
    public function setAmountOfItems($val)
    {
        $val = intval($val);

        if ($val <= 0) {
            throw new Exception('tx_mksearch_scheduler_TaskTriggerIndexingQueue->setAmountOfItems(): Invalid amount of items given!');
        }
        // else
        $this->amountOfItemsToIndexPerRun = $val;
    }

    /**
     * @return mixed
     */
    protected function getItemsInQueue()
    {
        return tx_mksearch_util_ServiceRegistry::getIntReindexService()->countItemsInQueue();
    }

    /**
     * @param $indexer
     * @return mixed
     */
    protected function getTableNameByIndexer($indexer) {
        $className = get_class($indexer);
        return $GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['mksearch']['ARRAY_MAPPING_INDEXER_DATABASE_TABLES'][$className];
    }

}

if (defined('TYPO3_MODE') && $GLOBALS['TYPO3_CONF_VARS'][TYPO3_MODE]['XCLASS']['ext/mksearch/scheduler/class.tx_mksearch_scheduler_ReindexAbstractTask.php']) {
    include_once $GLOBALS['TYPO3_CONF_VARS'][TYPO3_MODE]['XCLASS']['ext/mksearch/scheduler/class.tx_mksearch_scheduler_ReindexAbstractTask.php'];
}
