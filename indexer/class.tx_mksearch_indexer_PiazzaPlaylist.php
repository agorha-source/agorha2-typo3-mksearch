<?php

/**
 * Class tx_mksearch_indexer_PiazzaPlaylist
 * Service d'indexation appelé par l'extension "mksearch".
 * @author Sword SSl
 */
class tx_mksearch_indexer_PiazzaPlaylist extends tx_mksearch_indexer_BasePiazza {
    const STRING_UID_PREFIX = 'cms_playlist_';

    /**
     * Retourne l'identification du type de contenu.
     * Cette identification fait partie des données indexées et est utilisée lors de recherches ultérieures
     * pour identifier les résultats de la recherche. Vous êtes complètement libre dans la plage de valeurs,
     * mais faites attention car vous êtes en même temps responsable de l'unicité (c'est-à-dire pas de
     * chevauchement avec d'autres types de contenu) et de la cohérence (c'est-à-dire la reconnaissance)
     * sur l'indexation et la recherche de données.
     *
     * @return array
     */
    public static function getContentType()
    {
        return array('piazza_video', 'playlist');
    }

    /**
     * (non-PHPdoc).
     *
     * @see tx_mksearch_interface_Indexer::prepareSearchData()
     */
    public function prepareSearchData($tableName, $rawData, tx_mksearch_interface_IndexerDocument $indexDoc, $options)
    {
        if ('tx_piazzavideo_domain_model_playlist' != $tableName) {
            if (tx_rnbase_util_Logger::isWarningEnabled()) {
                tx_rnbase_util_Logger::warn(__METHOD__ . ': Unknown table "' . $tableName . '" given.', 'mksearch', array('tableName' => $tableName));
            }
            return null;
        }
        if (!$this->isIndexableRecord($rawData, $options)) {
            return null; //no need to index
        }
        if ($this->stopIndexing($tableName, $rawData, $indexDoc, $options)) {
            return null;
        }

        $objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager');
        /** @var \Sword\PiazzaVideo\Domain\Repository\PlaylistRepository $repository */
        $repository = $objectManager->get(\Sword\PiazzaVideo\Domain\Repository\PlaylistRepository::class);

        $record = $repository->findAllRecordsByUid($rawData['uid'], $rawData['pid']);
        if (!$record instanceof \Sword\PiazzaVideo\Domain\Model\Playlist) {
            throw new Exception("Playlist introuvable. Uid: {$rawData['uid']}", 404);
        }

        /*
         * Indexation des champs
         */
        $indexDoc->setUid(self::STRING_UID_PREFIX . $rawData['uid']);
        $indexDoc->addField('id', self::STRING_UID_PREFIX . $rawData['uid']);

        $indexDoc->addField('l10n_parent', $rawData['l10n_parent']);
        $indexDoc->addField('sys_language_uid', $rawData['sys_language_uid']);
        $indexDoc->setTimestamp($rawData['tstamp']);

        $indexDoc->addField('etl_processed', false);

        if ($this->hasDocToBeDeleted($rawData, $options)) {
            $indexDoc->addField('deleted', true);
            return $indexDoc;
        }
        else {
            $indexDoc->addField('deleted', false);
        }

        $indexDoc->addField('hidden', ($rawData['hidden']) ? (bool)$rawData['hidden'] : false);
        $indexDoc->addField('starttime', $rawData['starttime']);
        $indexDoc->addField('endtime', $rawData['endtime']);

        $indexDoc->addField('title', $this->getProperty($record, 'title'));
        $indexDoc->addField('datetime', $this->getProperty($record, 'datetime'));
        $indexDoc->addField('duree', $this->getProperty($record, 'duree'));
        $indexDoc->addField('description', $this->getProperty($record, 'description'));
        //$indexDoc->addField('description2', $this->getProperty($record, 'description2'));
        $indexDoc->addField('videos', $this->getProperty($record, 'videos'));
        $indexDoc->addField('categorie', $this->getProperty($record, 'categorie'));
        $indexDoc->addField('image_paysage', $this->getProperty($record, 'image_paysage'));
        $indexDoc->addField('image_carree', $this->getProperty($record, 'image_carree'));

        return $indexDoc;
    }

    /**
     * @param \Sword\PiazzaVideo\Domain\Model\Video $record
     * @return string
     */
    protected function getVideos($record)
    {
        return 'cms_video_' . $record->getUid();
    }

    /**
     * Shall we break the indexing for the current data?
     *
     * when an indexer is configured for more than one table
     * the index process may be different for the tables.
     * overwrite this method in your child class to stop processing and
     * do something different like putting a record into the queue
     * if it's not the table that should be indexed
     *
     * @param string $tableName
     * @param array $sourceRecord
     * @param tx_mksearch_interface_IndexerDocument $indexDoc
     * @param array $options
     *
     * @return bool
     */
    protected function stopIndexing(
        $tableName,
        $sourceRecord,
        tx_mksearch_interface_IndexerDocument $indexDoc,
        $options
    ) {
        $stopIndexing = parent::stopIndexing($tableName, $sourceRecord, $indexDoc, $options);

        $relatedTablesMapping = [
            [
                'main' => '	tx_piazzavideo_domain_model_categorie',
                'mm' => ['tx_piazzavideo_playlist_categorie_mm'],
                'hierarchicalField' => false
            ],
        ];

        if($this->checkRelatedData($tableName, $sourceRecord['uid'], $relatedTablesMapping)){
            $stopIndexing = true;
        }

        return $stopIndexing;
    }
}

if (defined('TYPO3_MODE') && $GLOBALS['TYPO3_CONF_VARS'][TYPO3_MODE]['XCLASS']['ext/mksearch/indexer/class.tx_mksearch_indexer_PiazzaPlaylist.php']) {
    include_once $GLOBALS['TYPO3_CONF_VARS'][TYPO3_MODE]['XCLASS']['ext/mksearch/indexer/class.tx_mksearch_indexer_PiazzaPlaylist.php'];
}
