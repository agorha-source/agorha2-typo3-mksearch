<?php

use DMK\Mksearch\Utility\PiazzaRepositoryUtility;

/**
 * Class tx_mksearch_indexer_PiazzaPage
 * Service d'indexation appelé par l'extension "mksearch".
 * @author Sword SSl
 */
class tx_mksearch_indexer_PiazzaPage extends tx_mksearch_indexer_BasePiazza {
    const STRING_UID_PREFIX = 'cms_page_';

    /**
     * Retourne l'identification du type de contenu.
     * Cette identification fait partie des données indexées et est utilisée lors de recherches ultérieures
     * pour identifier les résultats de la recherche. Vous êtes complètement libre dans la plage de valeurs,
     * mais faites attention car vous êtes en même temps responsable de l'unicité (c'est-à-dire pas de
     * chevauchement avec d'autres types de contenu) et de la cohérence (c'est-à-dire la reconnaissance)
     * sur l'indexation et la recherche de données.
     *
     * @return array
     */
    public static function getContentType()
    {
        return array('core', 'page');
    }

    /**
     * (non-PHPdoc).
     *
     * @see tx_mksearch_interface_Indexer::prepareSearchData()
     */
    public function prepareSearchData($tableName, $rawData, tx_mksearch_interface_IndexerDocument $indexDoc, $options)
    {
        if ('pages' != $tableName) {
            if (tx_rnbase_util_Logger::isWarningEnabled()) {
                tx_rnbase_util_Logger::warn(__METHOD__ . ': Unknown table "' . $tableName . '" given.', 'mksearch', array('tableName' => $tableName, 'sourceRecord' => $sourceRecord));
            }
            return null;
        }
        if (!$this->isIndexableRecord($rawData, $options)) {
            return null; //no need to index
        }
        if ($this->stopIndexing($tableName, $rawData, $indexDoc, $options)) {
            return null;
        }

        $objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager');
        /* @var \TYPO3\CMS\Frontend\Page\PageRepository $repository */
        $repository = $objectManager->get(\TYPO3\CMS\Frontend\Page\PageRepository::class);
        $record = $repository->getPage($rawData['uid'], true);
        if (!is_array($record) || empty($record)) {
            throw new Exception("Page introuvable. Uid: {$rawData['uid']}", 404);
        }

        /*
         * Indexation des champs
         */
        $indexDoc->setUid(self::STRING_UID_PREFIX . $rawData['uid']);
        $indexDoc->addField('id', self::STRING_UID_PREFIX . $rawData['uid']);

        $indexDoc->addField('l10n_parent', $rawData['l10n_parent']);
        $indexDoc->addField('sys_language_uid', $rawData['sys_language_uid']);
        $indexDoc->setTimestamp($rawData['tstamp']);

        $indexDoc->addField('etl_processed', false);

        if ($this->hasDocToBeDeleted($rawData, $options)) {
            $indexDoc->addField('deleted', true);
            return $indexDoc;
        }
        else {
            $indexDoc->addField('deleted', false);
        }

        $indexDoc->addField('hidden', (bool)$rawData['hidden']);
        $indexDoc->addField('starttime', $rawData['starttime']);
        $indexDoc->addField('endtime', $rawData['endtime']);

        $indexDoc->addField('title', $rawData['title']);
        $indexDoc->addField('subtitle', $rawData['subtitle']);
        $indexDoc->addField('chapeau', $rawData['chapeau']);
        $indexDoc->addField('vignette', PiazzaRepositoryUtility::getObject(
            $record,
            PiazzaRepositoryUtility::STRING_LAZY_FILE,
            ['tablenames' => $tableName, 'fieldname' => 'thumbnail']
        ));

        $indexDoc->addField('keywords', $this->splitKeywords($rawData['keywords']));

        //$indexDoc->addField('content_elements', $this->getProperty($record,'content_elements'));
        return $indexDoc;
    }

    /**
     * @param string $rawKeywords
     * @return array
     */
    private function splitKeywords($rawKeywords, $separatorPattern = '/,|\r\n|\n/')
    {
        $keywords = preg_split($separatorPattern, trim($rawKeywords));
        foreach ($keywords as $key => $keyword) {
            $keywords[$key] = trim($keyword);
        }
        return $keywords;
    }

    /**
     * Shall we break the indexing for the current data?
     *
     * when an indexer is configured for more than one table
     * the index process may be different for the tables.
     * overwrite this method in your child class to stop processing and
     * do something different like putting a record into the queue
     * if it's not the table that should be indexed
     *
     * @param string $tableName
     * @param array $sourceRecord
     * @param tx_mksearch_interface_IndexerDocument $indexDoc
     * @param array $options
     *
     * @return bool
     */
    protected function stopIndexing(
        $tableName,
        $sourceRecord,
        tx_mksearch_interface_IndexerDocument $indexDoc,
        $options
    ) {
        return parent::stopIndexing(
            $tableName,
            $sourceRecord,
            $indexDoc,
            $options
        );
    }

}

if (defined('TYPO3_MODE') && $GLOBALS['TYPO3_CONF_VARS'][TYPO3_MODE]['XCLASS']['ext/mksearch/indexer/class.tx_mksearch_indexer_PiazzaPage.php']) {
    include_once $GLOBALS['TYPO3_CONF_VARS'][TYPO3_MODE]['XCLASS']['ext/mksearch/indexer/class.tx_mksearch_indexer_PiazzaPage.php'];
}
