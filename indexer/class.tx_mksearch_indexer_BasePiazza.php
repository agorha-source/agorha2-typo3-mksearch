<?php

use DMK\Mksearch\Utility\PiazzaRepositoryUtility;
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

/**
 * Class tx_mksearch_indexer_BasePiazza
 * Common methods for Piazza Indexers (Repository based)
 * @author Sword SSl
 */
abstract class tx_mksearch_indexer_BasePiazza implements tx_mksearch_interface_Indexer {

    /**
     * @return array
     */
    abstract static function getContentType();

    /**
     * @param string $tableName
     * @param array $rawData
     * @param tx_mksearch_interface_IndexerDocument $indexDoc
     * @param array $options
     * @return tx_mksearch_interface_IndexerDocument|null
     */
    abstract function prepareSearchData($tableName,
                                        $rawData,
                                        tx_mksearch_interface_IndexerDocument $indexDoc,
                                        $options);

    /**
     * @param AbstractEntity $record
     * @param $property
     * @param null $specialGetter
     * @return array|stdClass|null
     */
    protected function getProperty($record, $property, $specialGetter = null, $specialGetterParameters = [])
    {
        $getterName = 'get' . PiazzaRepositoryUtility::underscoresToCamelCase($property);
        $recordProperty = $record->{$getterName}();
        if ($recordProperty instanceof \TYPO3\CMS\Extbase\Persistence\ObjectStorage) {
            // c'est une relation multivaluée
            $array = [];
            $subRecords = $recordProperty->toArray();
            if (isset($specialGetter)) {
                // getter est géré dans l'utilitaire
                foreach ($subRecords as $subRecord) {
                    $array[] = PiazzaRepositoryUtility::getObject($subRecord, $specialGetter, $specialGetterParameters);
                }
            }
            elseif (method_exists($this, $getterName)) {
                //getter est défini dans l'indexeur
                foreach ($subRecords as $subRecord) {
                    $array[] = $this->{$getterName}($subRecord);
                }
            }
            elseif ($recordProperty[0] instanceof \TYPO3\CMS\Extbase\Domain\Model\FileReference) {
                // c'est un fichier
                foreach ($subRecords as $subRecord) {
                    $array[] = PiazzaRepositoryUtility::getFileReferenceAsObject($subRecord);
                }
            }
            return (!empty($array)) ? $array : null;
        }
        else {
            // c'est une propriété/relation monovaluée
            if (isset($specialGetter)) {
                // getter est géré dans l'utilitaire
                $return = PiazzaRepositoryUtility::getObject($record, $specialGetter, $specialGetterParameters);
            }
            elseif (method_exists($this, $getterName)) {
                //getter est défini ici dans l'indexeur
                $return = $this->{$getterName}($record);
            }
            elseif ($recordProperty instanceof \TYPO3\CMS\Extbase\Domain\Model\FileReference) {
                // c'est un fichier
                $return = PiazzaRepositoryUtility::getFileReferenceAsObject($recordProperty);
            }
            else {
                // c'est une propriété simple (pas une relation), on utilise le getter du model Extbase
                $return = $recordProperty;
            }
            return $return;
        }
    }

    /**
     * @param $tableName
     * @param $sourceRecord
     * @param tx_mksearch_interface_IndexerDocument $indexDoc
     * @param $options
     * @return mixed
     */
    protected function stopIndexing(
        $tableName,
        $sourceRecord,
        tx_mksearch_interface_IndexerDocument $indexDoc,
        $options
    ) {
        return $this->getIndexerUtility()->stopIndexing(
            $tableName,
            $sourceRecord,
            $indexDoc,
            $options
        );
    }

    /**
     * @param $tableName
     * @param $sourceRecordUid
     * @param $relatedTablesMapping
     * @return bool
     */
    protected function checkRelatedData($tableName, $sourceRecordUid, $relatedTablesMapping){
        $isRelatedData = false;
        // browse $relatedTablesMapping to reach the concerned table
        foreach($relatedTablesMapping as $relatedTable){
            if ($tableName === $relatedTable['main']) {
                // gestion des alias (meme entité liée via une propriété différente)
                foreach($relatedTable['mm'] as $mm){
                    // index related content
                    $this->handleRelatedDataChanged($sourceRecordUid, $relatedTable['main'], $mm, $relatedTable['hierarchicalField']);
                }
                // stop indexing current record
                $isRelatedData = true;
                // exit loop
                break;
            }
        }
        return $isRelatedData;
    }

    /**
     * Handle data change for a related table. All connected content (mainTable) should be updated.
     * @param int $uid
     * @param string $mainTable
     * @param array $mmTable
     * @param bool $hierarchicalReferenceField
     * @return void
     */
    protected function handleRelatedDataChanged($uid, $mainTable, $mmTable, $hierarchicalReferenceField = false)
    {
        // select connected content
        $rows = $this->getDatabaseConnection()->doSelect(
            "MAIN.uid AS connectedUid",
            [
                "{$mainTable} AS MAIN " .
                "JOIN {$mmTable} AS MM ON MAIN.uid = MM.uid_foreign",
                "{$mainTable}",
                "MAIN",
            ],
            [
                'WHERE' => "MM.uid_local = {$uid}",
            ]
        );

        // reindex connected content
        $srv = $this->getIntIndexService();
        foreach ($rows as $row) {
            $srv->addRecordToIndex($mainTable, (int)$row['connectedUid']);
        }

        // if hierarchical relation, do recursive
        if ($hierarchicalReferenceField) {
            // check if children, then reindex connected content
            $rows = $this->getDatabaseConnection()->doSelect(
                "uid AS children",
                $mainTable,
                [
                    'WHERE' => "$hierarchicalReferenceField = {$uid}"
                ]
            );
            foreach ($rows as $row) {
                $this->handleRelatedDataChanged($row['children'], $mainTable, $mmTable);
            }
        }
    }

    /**
     * @param array $sourceRecord
     * @param array $options
     *
     * @return bool
     */
    protected function isIndexableRecord($sourceRecord, $options)
    {
        return tx_mksearch_util_Indexer::getInstance()
            ->isOnIndexablePage($sourceRecord, $options);
    }

    /**
     * The index Service.
     *
     * @return tx_mksearch_service_internal_Index
     */
    protected function getIntIndexService()
    {
        return tx_mksearch_util_ServiceRegistry::getIntIndexService();
    }

    /**
     * The conection to the db.
     *
     * @return Tx_Rnbase_Database_Connection
     */
    protected function getDatabaseConnection()
    {
        return Tx_Rnbase_Database_Connection::getInstance();
    }

    /**
     * @return tx_mksearch_util_Indexer
     */
    protected function getIndexerUtility()
    {
        return tx_mksearch_util_Indexer::getInstance();
    }

  /**
   * Vérifie si l'élément donné est bien lié
   *
   * @param $tableName
   * @param $sourceRecordUid
   * @param $relatedTablesMapping
   * @return bool
   */
  protected function checkIfDataIsRelatedAndUpdate($tableName, $sourceRecordUid, $relatedTablesMapping)
  {
    $isRelatedData = false;
    // Récupération de chaque table présent dans le tableau
    foreach($relatedTablesMapping as $relatedTable){
      // Si la table en cours de modification fait bien partie des tables liées à surveiller
      if ($tableName === $relatedTable['main']) {
        // On vérifie que l'élément modifié était bien lié et si oui on renvoie tous les ids des news à mettre à jour
        $newsIds = $this->getAllIdsOfNewsToUpdate($sourceRecordUid,$relatedTable['mm'][0]);
        if($newsIds && is_array($newsIds)) {
          // reindex connected content
          $srv = $this->getIntIndexService();
          foreach ($newsIds as $newsId) {
            $srv->addRecordToIndex('tx_news_domain_model_news', (int)$newsId['uid_local']);
          }
        }
        $isRelatedData = true;
        // exit loop
        break;
      }
    }
    return $isRelatedData;
  }

  /**
   * Retourne un tableau des news liés à $sourceRecordUid
   * @param $sourceRecordUid
   * @param $relatedTable
   * @return mixed
   */
  protected function getAllIdsOfNewsToUpdate($sourceRecordUid, $relatedTable)
  {
    $queryBuilder = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Database\ConnectionPool::class)
      ->getQueryBuilderForTable($relatedTable);
    $statement = $queryBuilder
      ->select('*')
      ->from($relatedTable)
      ->where(
        $queryBuilder->expr()->eq($relatedTable.'.uid_foreign',$sourceRecordUid)
      )
      ->execute();
    return $statement->fetchAll();
  }

  /**
   * Permet de récupérer un content element de la base de données ayant l'id $databaseId
   *
   * @param $databaseId
   * @param $contentElement
   * @return mixed
   */
  protected function getContentElementFromDatabase($databaseId,$contentElement) {
    $queryBuilder = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Database\ConnectionPool::class)
      ->getQueryBuilderForTable('tt_content');
    $statement = $queryBuilder
      ->select('*')
      ->from('tt_content')
      ->where(
        $queryBuilder->expr()->eq('tt_content.pid', $queryBuilder->createNamedParameter($databaseId, \PDO::PARAM_INT)),
        $queryBuilder->expr()->eq('tt_content.CType', $queryBuilder->createNamedParameter($contentElement, \PDO::PARAM_STR))
      )
      ->execute();
    return $statement->fetch();
  }

  /**
   * Vérifie si le content element fait partie d'une base de données
   * @param $rawData
   * @return bool
   */
  protected function contentElementIsContainedInDatabasePage($rawData) {
    $queryBuilder = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Database\ConnectionPool::class)
      ->getQueryBuilderForTable('pages');
    $statement = $queryBuilder
      ->select('*')
      ->from('pages')
      ->where(
        $queryBuilder->expr()->eq('pages.uid', $queryBuilder->createNamedParameter($rawData['pid'], \PDO::PARAM_INT)),
        $queryBuilder->expr()->eq('pages.pid',50)
      )
      ->execute();
    return $statement->fetch() ? true : false;
  }

  /**
     * Sets the index doc to deleted if neccessary.
     *
     * @param array $sourceRecord
     * @param array $options
     *
     * @return bool
     */
    protected function hasDocToBeDeleted($sourceRecord, $options)
    {
        if (// supprimer de l'indexeur si
            // deleted
            ($sourceRecord['deleted'])
            ||
            // hidden
            (isset($options['removeIfHidden']) && $options['removeIfHidden'] && $sourceRecord['hidden'])
        ) {
            return true;
        }
        //else
        return false;
    }

    /**
     * Renvoyer la configuration Typoscript par défaut pour cet indexeur.
     *
     * @return string
     */
    public function getDefaultTSConfig()
    {
        return <<<CONF
# Fields which are set statically to the given value
# Don't forget to add those fields to your Solr schema.xml
# For example it can be used to define site areas this
# contentType belongs to
#
# fixedFields {
#   my_fixed_field_singlevalue = first
#   my_fixed_field_multivalue {
#      0 = first
#      1 = second
#   }
# }

### supprimer de l'indexeur s'il est défini sur caché
removeIfHidden = 1
content {
  ### Champs qui sont écrits dans le champ de contenu de l'indexeur.
  fields = name,title,first_name,middle_name,last_name,address,zip,city,country,email,description
  ### Comment les champs sont-ils encapsulés pour le contenu?
  wrap = |
}
## voir le contenu, par défaut nous n'avons pas besoin d'un résumé, car ttaddress a son propre modèle
abstract {
  wrap = <span>|</span>
}

### delete from or abort indexing for the record if isIndexableRecord or no record?
 deleteIfNotIndexable = 0

### White lists:
include {
  ### Seules les entrées de ces pages sont indexées. (Séparé par des virgules)
  pages =
  ### Seules les entrées de ces pages sont indexées.
  pages {
    #0 = 415
  }
}
### Black lists
exclude {
  ### Seules les entrées de ces pages sont indexées. (Séparé par des virgules)
  pages =
  ### Seules les entrées de ces pages sont indexées.
  pages {
    #0 = 415
  }
}

# you should always configure the root pageTree for this indexer in the includes. mostly the domain
# include.pageTrees {
#   0 = $pid-of-domain
# }

# should a special workspace be indexed?
# default is the live workspace (ID = 0)
# comma separated list of workspace IDs
#workspaceIds = 1,2,3

# \$sys_language_uid of the desired language
lang = 0,1,2
CONF;
    }
}
