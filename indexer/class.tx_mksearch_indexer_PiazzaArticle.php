<?php

use DMK\Mksearch\Utility\PiazzaRepositoryUtility;

/**
 * Class tx_mksearch_indexer_PiazzaArticle
 * Service d'indexation appelé par l'extension "mksearch".
 * @author Sword SSl
 */
class tx_mksearch_indexer_PiazzaArticle extends tx_mksearch_indexer_BasePiazza {

    const STRING_UID_PREFIX = 'cms_article_';

    /**
     * Retourne l'identification du type de contenu.
     * Cette identification fait partie des données indexées et est utilisée lors de recherches ultérieures
     * pour identifier les résultats de la recherche. Vous êtes complètement libre dans la plage de valeurs,
     * mais faites attention car vous êtes en même temps responsable de l'unicité (c'est-à-dire pas de
     * chevauchement avec d'autres types de contenu) et de la cohérence (c'est-à-dire la reconnaissance)
     * sur l'indexation et la recherche de données.
     *
     * @return array
     */
    public static function getContentType()
    {
        return array('piazza_magazine', 'article');
    }

    /**
     * @param string $tableName
     * @param array $rawData
     * @param tx_mksearch_interface_IndexerDocument $indexDoc
     * @param array $options
     * @return tx_mksearch_interface_IndexerDocument|null
     * @throws Exception
     *
     * @see tx_mksearch_interface_Indexer::prepareSearchData()
     */
    public function prepareSearchData($tableName, $rawData, tx_mksearch_interface_IndexerDocument $indexDoc, $options)
    {
        if (
            'tx_news_domain_model_news' !== $tableName
            && 'sys_category' !== $tableName
            && 'tx_piazzamotscles_domain_model_motcle' !== $tableName
        ) {
            if (tx_rnbase_util_Logger::isWarningEnabled()) {
                tx_rnbase_util_Logger::warn(__METHOD__ . ': Unknown table "' . $tableName . '" given.', 'mksearch', array('tableName' => $tableName, 'rawData' => $rawData));
            }
            return null;
        }
        if (!$this->isIndexableRecord($rawData, $options)) {
            return null; //no need to index
        }
        if ($this->stopIndexing($tableName, $rawData, $indexDoc, $options)) {
            return null;
        }

        $objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager');
        /* @var \GeorgRinger\News\Domain\Repository\NewsRepository $repository */
        $repository = $objectManager->get(\GeorgRinger\News\Domain\Repository\NewsRepository::class);

        $record = $repository->findAllRecordsByUid($rawData['uid'], $rawData['pid']);
        if (!$record instanceof \GeorgRinger\News\Domain\Model\News) {
            throw new Exception("Article introuvable. Uid: {$rawData['uid']}", 404);
        }

        /*
         * Indexation des champs
         */
        $indexDoc->setUid(self::STRING_UID_PREFIX . $rawData['uid']);
        $indexDoc->addField('id', self::STRING_UID_PREFIX . $rawData['uid']);

        $indexDoc->addField('l10n_parent', $rawData['l10n_parent']);
        $indexDoc->addField('sys_language_uid', $rawData['sys_language_uid']);
        $indexDoc->setTimestamp($rawData['tstamp']);

        $indexDoc->addField('etl_processed', false);

        if ($this->hasDocToBeDeleted($rawData, $options)) {
            $indexDoc->addField('deleted', true);
            return $indexDoc;
        }
        else {
            $indexDoc->addField('deleted', false);
        }

        $indexDoc->addField('hidden', (bool)$rawData['hidden']);
        $indexDoc->addField('starttime', $rawData['starttime']);
        $indexDoc->addField('endtime', $rawData['endtime']);

        $indexDoc->addField('title', $this->getProperty($record, 'title'));
        $indexDoc->addField('teaser', $this->getProperty($record, 'teaser'));
        $indexDoc->addField('datetime', $this->getProperty($record, 'datetime'));
        $indexDoc->addField('motscles', $this->getProperty($record, 'motscles'));
        $indexDoc->addField('author', $this->getProperty($record, 'author'));
        $indexDoc->addField('fal_media', $this->getProperty($record, 'fal_media'));
        $indexDoc->addField('fal_related_files', $this->getProperty($record, 'fal_related_files'));
        $indexDoc->addField('categories', $this->getProperty($record, 'categories', PiazzaRepositoryUtility::STRING_H_CONCATENATED));
        $indexDoc->addField('categories_first_level', $this->getProperty($record, 'categories', PiazzaRepositoryUtility::STRING_H_FIRST_LEVEL));
        $indexDoc->addField('categories_last_level', $this->getProperty($record, 'categories', PiazzaRepositoryUtility::STRING_H_LAST_LEVEL));

        $indexDoc->addField('tempslecture', $this->getProperty($record, 'tempslecture'));
        $indexDoc->addField('modiftime', $this->getProperty($record, 'modiftime'));
        $indexDoc->addField('bodytext', $this->getProperty($record, 'bodytext'));
        $indexDoc->addField('notebasdepage', $this->getProperty($record, 'notebasdepage'));
        $indexDoc->addField('copyright', $this->getProperty($record, 'copyright'));
        $indexDoc->addField('infobasdepage', $this->getProperty($record, 'infobasdepage'));
        $indexDoc->addField('related', $this->getProperty($record, 'related', PiazzaRepositoryUtility::STRING_PARENT_RELATION, ['uid_prefix' => self::STRING_UID_PREFIX]));
        $indexDoc->addField('titreauteur', $this->getProperty($record, 'titreauteur'));
        $indexDoc->addField('precisionsauteur', $this->getProperty($record, 'precisionsauteur'));
        $indexDoc->addField('portraitauteur', $this->getProperty($record, 'portraitauteur'));

        return $indexDoc;
    }

    /**
     * @param \Sword\PiazzaMotscles\Domain\Model\Motcle $property
     * @return string
     */
    protected function getMotscles($property)
    {
        return $property->getLibelle();
    }

    /**
     * @param \GeorgRinger\News\Domain\Model\News $property
     * @return int
     */
    protected function getRelated($property)
    {
        return $property->getUid();
    }

    /**
     * Shall we break the indexing for the current data?
     *
     * when an indexer is configured for more than one table
     * the index process may be different for the tables.
     * overwrite this method in your child class to stop processing and
     * do something different like putting a record into the queue
     * if it's not the table that should be indexed
     *
     * @param string $tableName
     * @param array $sourceRecord
     * @param tx_mksearch_interface_IndexerDocument $indexDoc
     * @param array $options
     *
     * @return bool
     */
    protected function stopIndexing(
        $tableName,
        $sourceRecord,
        tx_mksearch_interface_IndexerDocument $indexDoc,
        $options
    ) {
        $stopIndexing = parent::stopIndexing($tableName, $sourceRecord, $indexDoc, $options);

        $relatedTablesMapping = [
            [
                'main' => 'sys_category',
                'mm' => ['sys_category_record_mm'],
                'hierarchicalField' => 'parent'
            ],
            [
                'main' => 'tx_piazzamotscles_domain_model_motcle',
                'mm' => ['tx_news_domain_model_news_tag_mm'],
                'hierarchicalField' => false
            ],
        ];

        if($this->checkRelatedData($tableName, $sourceRecord['uid'], $relatedTablesMapping)){
            $stopIndexing = true;
        }

        return $stopIndexing;
    }

}

if (defined('TYPO3_MODE') && $GLOBALS['TYPO3_CONF_VARS'][TYPO3_MODE]['XCLASS']['ext/mksearch/indexer/class.tx_mksearch_indexer_PiazzaArticle.php']) {
    include_once $GLOBALS['TYPO3_CONF_VARS'][TYPO3_MODE]['XCLASS']['ext/mksearch/indexer/class.tx_mksearch_indexer_PiazzaArticle.php'];
}