<?php

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ServerException;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class tx_mksearch_indexer_AgorhaPage
 * Service d'indexation appelé par l'extension "mksearch".
 * @author Sword SSl
 */
class tx_mksearch_indexer_AgorhaPage extends tx_mksearch_indexer_BasePiazza
{

  /**
   * Retourne l'identification du type de contenu.
   * Cette identification fait partie des données indexées et est utilisée lors de recherches ultérieures
   * pour identifier les résultats de la recherche. Vous êtes complètement libre dans la plage de valeurs,
   * mais faites attention car vous êtes en même temps responsable de l'unicité (c'est-à-dire pas de
   * chevauchement avec d'autres types de contenu) et de la cohérence (c'est-à-dire la reconnaissance)
   * sur l'indexation et la recherche de données.
   *
   * @return array
   */
  public static function getContentType()
  {
    return array('core', 'page');
  }

  /**
   * (non-PHPdoc).
   *
   * @param $tableName
   * @param $rawData
   * @param tx_mksearch_interface_IndexerDocument $indexDoc
   * @param $options
   * @return tx_mksearch_interface_IndexerDocument|null
   * @see tx_mksearch_interface_Indexer::prepareSearchData()
   */
  public function prepareSearchData($tableName, $rawData, tx_mksearch_interface_IndexerDocument $indexDoc, $options)
  {
    $databasesUtility = new \Sword\AgorhaBase\Utility\DatabasesUtility();

    if ('pages' != $tableName && 'tt_content' != $tableName) {
      if (tx_rnbase_util_Logger::isWarningEnabled()) {
        tx_rnbase_util_Logger::warn(__METHOD__ . ': Unknown table "' . $tableName . '" given.', 'mksearch', array('tableName' => $tableName, 'sourceRecord' => $sourceRecord));
      }
      return null;
    }
    if ($this->stopIndexing($tableName, $rawData, $indexDoc, $options)) {
      return null;
    }

    $databaseHeader = $this->getContentElementFromDatabase($rawData['uid'],'mask_database_header');
    $databaseUniqueKey = $this->getDatabaseUniqueKey($databaseHeader);
    // Est-ce que la base de données existe déjà
    $currentDatabase = $databasesUtility->getDatabaseInfo($databaseUniqueKey);
    if ($databaseHeader && ($rawData['pid'] == 50)) {
      // Nécessaire pour l'indexation
      $indexDoc->setUid($currentDatabase ? $currentDatabase->id : null);

      /*
       * Indexation des champs
       */
      $database = new StdClass();
      $database->internal = new StdClass();
      $database->internal->createdDate = $currentDatabase ? $currentDatabase->internal->createdDate : date("Y-m-d\TH:i:s", $rawData['crdate']);
      $database->internal->createdBy = $currentDatabase ? $currentDatabase->internal->createdBy : $databaseHeader['tx_mask_database_author'];
      $database->internal->updatedDate = date("Y-m-d\TH:i:s", $rawData['tstamp']);
      $database->internal->updatedBy = $databaseHeader['tx_mask_database_author'];
      $database->internal->uuid = $currentDatabase ? $currentDatabase->internal->uuid : (string)$databaseUniqueKey;
      $database->internal->uniqueKey = $currentDatabase ? $currentDatabase->internal->uniqueKey : (int)$databaseUniqueKey;
      $database->internal->agorhaStatus = (int)$this->getStatus($rawData);
      $database->internal->cmsStatus = (int)$databaseHeader['tx_mask_database_status'];
      $database->internal->permalink = $rawData['slug'];
      $database->internal->relatedNoticeType = $this->getDatabaseType($databaseHeader['tx_mask_databasetype']);

      $database->content = new StdClass();
      $database->content->title = $databaseHeader['tx_mask_title'];
      $database->content->picture = $this->getDatabaseImage($databaseHeader['uid']);

      $description = $this->getContentElementFromDatabase($rawData['uid'],'mask_database_description')['tx_mask_database_description'];
      $selection = $this->getContentElementFromDatabase($rawData['uid'],'mask_notices_list')['tx_mask_notice_selection'];
      $database->content->description = $description ? $description : '';
      $database->content->selection = $selection ? $selection : '';

      $indexDoc->addField('internal', $database->internal);
      $indexDoc->addField('content', $database->content);
    }

    return $indexDoc;
  }

  /**
   * Récupère l'image de l'en-tête de base de données
   *
   * @param $contentElementUid
   * @return mixed
   */
  protected function getDatabaseImage($contentElementUid)
  {
    $fileRepository = GeneralUtility::makeInstance(\TYPO3\CMS\Core\Resource\FileRepository::class);
    $fileObjects = $fileRepository->findByRelation('tt_content', 'tx_mask_database_image', $contentElementUid);
    $fileObject = $fileObjects[0]->getOriginalFile();
    return $fileObject->getIdentifier();
  }

  /**
   * Si l'unique key a été renseignée on la récupère sinon on la créée
   * @param $databaseHeader
   * @return int
   */
  protected function getDatabaseUniqueKey($databaseHeader) {
    if ($databaseHeader['tx_mask_database_unique_key'] && $databaseHeader['tx_mask_database_unique_key'] !== 0) {
      return (int)$databaseHeader['tx_mask_database_unique_key'];
    } else {
      return null;
    }
  }

  /**
   * Si page est cachée ou supprimée -> Non publiée
   * Sinon -> Publiée
   * @param $rawData
   * @return int
   */
  protected function getStatus($rawData) {
    if ($rawData['deleted'] == 1 || $rawData['hidden'] == 1 ) {
      return 6;
    } else {
      return 7;
    }
  }

  protected function getDatabaseType($databaseType) {
    $types = explode(',',$databaseType);
    $typeArray = [];
    for($i=0;$i<count($types);$i++){
      $string = '';
      switch ($types[$i]) {
        case '1':{
          $string = 'ARTWORK';
          break;
        }
        case '2':{
          $string = 'PERSON';
          break;
        }
        case '3':{
          $string = 'REFERENCE';
          break;
        }
        case '4':{
          $string = 'EVENT';
          break;
        }
        case '5':{
          $string = 'COLLECTION';
          break;
        }
      }
      array_push($typeArray,$string);
    }
    return $typeArray;
  }

  /**
   * Shall we break the indexing for the current data?
   *
   * when an indexer is configured for more than one table
   * the index process may be different for the tables.
   * overwrite this method in your child class to stop processing and
   * do something different like putting a record into the queue
   * if it's not the table that should be indexed
   *
   * @param string $tableName
   * @param array $sourceRecord
   * @param tx_mksearch_interface_IndexerDocument $indexDoc
   * @param array $options
   *
   * @return bool
   */
  protected function stopIndexing(
    $tableName,
    $rawData,
    tx_mksearch_interface_IndexerDocument $indexDoc,
    $options
  )
  {
    $stopIndexing = parent::stopIndexing($tableName, $rawData, $indexDoc, $options);
    if (
      $tableName == 'tt_content' &&
      ($rawData['CType'] == 'mask_database_header'|| $rawData['CType'] == 'mask_database_description' || $rawData['CType'] == 'mask_notices_list') &&
      $this->contentElementIsContainedInDatabasePage($rawData)
    ) {
      $srv = $this->getIntIndexService();
      $srv->addRecordToIndex('pages', (int)$rawData['pid']);
      $stopIndexing = true;
    }
    // Si template base de données modifié - pas d'indexation
    if((int)$rawData['uid'] === 68) {
      $stopIndexing = true;
    }
    return $stopIndexing;
  }

}

if (defined('TYPO3_MODE') && $GLOBALS['TYPO3_CONF_VARS'][TYPO3_MODE]['XCLASS']['ext/mksearch/indexer/class.tx_mksearch_indexer_AgorhaPage.php']) {
  include_once $GLOBALS['TYPO3_CONF_VARS'][TYPO3_MODE]['XCLASS']['ext/mksearch/indexer/class.tx_mksearch_indexer_AgorhaPage.php'];
}
