
<?php

use DMK\Mksearch\Utility\PiazzaRepositoryUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class tx_mksearch_indexer_AgorhaNews
 * Service d'indexation appelé par l'extension "mksearch".
 * @author Sword SSl
 */
class tx_mksearch_indexer_AgorhaNews extends tx_mksearch_indexer_BasePiazza {

  const STRING_UID_PREFIX = 'cms_news_';

  /**
   * Retourne l'identification du type de contenu.
   * Cette identification fait partie des données indexées et est utilisée lors de recherches ultérieures
   * pour identifier les résultats de la recherche. Vous êtes complètement libre dans la plage de valeurs,
   * mais faites attention car vous êtes en même temps responsable de l'unicité (c'est-à-dire pas de
   * chevauchement avec d'autres types de contenu) et de la cohérence (c'est-à-dire la reconnaissance)
   * sur l'indexation et la recherche de données.
   *
   * @return array
   */
  public static function getContentType()
  {
    return array('agorha_news', 'news');
  }

  /**
   * @param string $tableName
   * @param array $rawData
   * @param tx_mksearch_interface_IndexerDocument $indexDoc
   * @param array $options
   * @return tx_mksearch_interface_IndexerDocument|null
   * @throws Exception
   *
   * @see tx_mksearch_interface_Indexer::prepareSearchData()
   */
  public function prepareSearchData($tableName, $rawData, tx_mksearch_interface_IndexerDocument $indexDoc, $options)
  {
    if (
      'tx_news_domain_model_news' !== $tableName
      && 'tx_news_domain_model_tag' !== $tableName
    ) {
      if (tx_rnbase_util_Logger::isWarningEnabled()) {
        tx_rnbase_util_Logger::warn(__METHOD__ . ': Unknown table "' . $tableName . '" given.', 'mksearch', array('tableName' => $tableName, 'rawData' => $rawData));
      }
      return null;
    }
    if (!$this->isIndexableRecord($rawData, $options)) {
      return null; //no need to index
    }
    if ($this->stopIndexing($tableName, $rawData, $indexDoc, $options)) {
      return null;
    }

    $objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager');
    /* @var \GeorgRinger\News\Domain\Repository\NewsRepository $repository */
    $repository = $objectManager->get(\GeorgRinger\News\Domain\Repository\NewsRepository::class);
    $record = $repository->findByUid($rawData['uid'], false);

    // Obligatoire pour l'indexation
    $indexDoc->setUid(self::STRING_UID_PREFIX . $rawData['uid']);

    /*
     * Indexation des champs
     */
    $news = new StdClass();
    // Internal
    $news->internal = new StdClass();
    $news->internal->createdDate = date("Y-m-d\TH:i:s", $rawData['crdate']);
    $news->internal->createdBy = $rawData['author'];
    $news->internal->updatedDate = date("Y-m-d\TH:i:s", $rawData['tstamp']);
    $news->internal->uuid = (string)$rawData['uid'];
    $news->internal->status = $this->getNewsStatus($rawData['status'],$rawData);
    $news->internal->noticeType = "NEWS";

    // Digest
    $news->internal->digest = new StdClass();
    $news->internal->digest->createdDate = date("Y-m-d\TH:i:s", $rawData['crdate']);
    $news->internal->digest->createdDatePath = [date("/Y").date("/Y-m-d", $rawData['crdate'])];
    $news->internal->digest->updatedDate = date("Y-m-d\TH:i:s", $rawData['tstamp']);
    $news->internal->digest->title = $rawData['title'];
    $news->internal->digest->newsType = $rawData['agorha_news_type'];
    $news->internal->digest->description = html_entity_decode($rawData['news_description']);
    $news->internal->digest->headerMedia = $this->getNewsImage($rawData['uid']);
    $news->internal->digest->noticeType = 'NEWS';
    $news->internal->digest->permalink = "/detail/" . $rawData['uid'];

    if ($record) {
      $news->internal->digest->databases = $this->getRelatedDatabases($this->getProperty($record, 'related_pages',PiazzaRepositoryUtility::STRING_PARENT_RELATION), true);
    }

    // Content
    $news->content = new StdClass();
    $news->content->bibliographie = $rawData['bibliographie'];
    $news->content->biographie = $rawData['biographie'];
    $news->content->headerMedia = $this->getNewsImage($rawData['uid']);
    $news->content->description = html_entity_decode($rawData['news_description']);
    $news->content->title = $rawData['title'];
    $news->content->newsType = $rawData['agorha_news_type'];

    if ($record) {
      $news->content->tags = $this->getNewsTags($record, $indexDoc);
      $news->content->selection = $this->getNewsSelection($record);
      $news->content->contentElements = $this->getNewsTextElements($record);
      $news->content->databases = $this->getRelatedDatabases($this->getProperty($record, 'related_pages',PiazzaRepositoryUtility::STRING_PARENT_RELATION), false);
    }

    // Si la news est archivée ou que le composant mask_notices_list n'est pas présent, on supprime le lien entre news et notice
    if ($news->internal->status != 'Publiée' || !$news->content->selection) {
      $this->removeNewsFromSelection($news->content->selection, $news->internal->digest->permalink);
    }
    // Si la news n'est pas archivée ou que le composant mask_notices_list est présent, on ajoute le lien entre news et notice
    else if ($news->internal->status == 'Publiée' && $news->content->selection) {
      $this->addNewsInSelection($news->content->selection->ref, $news->content->title, $news->internal->digest->permalink);
    }

    $indexDoc->addField('internal', $news->internal);
    $indexDoc->addField('content', $news->content);

    return $indexDoc;
  }

  /**
   * Récupère l'image de l'en-tête de l'article / actualité
   * Retourne le chemin de l'image ou null si la news a été supprimée
   *
   * @param $contentElementUid
   * @return mixed
   */
  protected function getNewsImage($contentElementUid)
  {
    $fileRepository = GeneralUtility::makeInstance(\TYPO3\CMS\Core\Resource\FileRepository::class);
    $fileObjects = $fileRepository->findByRelation('tx_news_domain_model_news', 'header_media', $contentElementUid);
    $fileObject = $fileObjects[0]->getOriginalFile();
    return $fileObject->getIdentifier() ? "/fileadmin" . $fileObject->getIdentifier() : null;
  }

  /**
   * Renvoie int status en string status
   *
   * @param $newsStatus
   * @param $rawData
   * @return string
   */
  protected function getNewsStatus($newsStatus, $rawData) {
    if($rawData['deleted'] == 1 || $rawData['hidden'] == 1) {
      $status = 'Archivée';
    } else {
      switch ($newsStatus) {
        case '0': {
          $status = 'En cours';
          break;
        }
        case '1': {
          $status = 'A publier';
          break;
        }
        case '2': {
          $status = 'Publiée';
          break;
        }
        case '3':
        case '4': {
          $status = 'Archivée';
          break;
        }
      }
    }
    return $status;
  }

  /**
   * Retourne la sélection de notice présente dans la news
   *
   * @param $news
   * @return StdClass
   */
  private function getNewsSelection($news) {
    $contents = $this->getNewsContentElements($news, 'mask_notices_list');
    if ($contents) {
      $selection = new StdClass();
      $contents = array_values($contents);
      $selection->ref = $contents[0]->getTxMaskNoticeSelection();
      $selection->value = $contents[0]->getTxMaskTitle();
      return $selection;
    } else {
      return null;
    }
  }

  /**
   * Retourne les éléments de type text
   *
   * @param $news
   * @return array
   */
  private function getNewsTextElements($news) {
    $contents = $news->getContentElements();
    $data = [];
    if($contents) {
      for($i=0;$i<count($contents);$i++) {
        if($contents[$i]){
          array_push($data, $contents[$i]->getHeader());
          array_push($data, $contents[$i]->getSubHeader());
          array_push($data, $contents[$i]->getBodytext());
        }
      }
      $data = array_values(array_filter($data));
    }
    return $data;
  }

  /**
   * Add category data of the News to the index.
   *
   * @param \GeorgRinger\News\Domain\Model\News $news
   * @param $contentElement
   * @return array
   */
  protected function getNewsContentElements(\GeorgRinger\News\Domain\Model\News $news,$contentElement) {
    $contentElements = $news->getContentElements();
    $return = [];
    if($contentElements) {
      for($i=0;$i<count($contentElements);$i++) {
        if($contentElement === $contentElements[$i]->getCType()) {
          $return[$i] = $contentElements[$i];
        }
      }
    }
    return $return;
  }

  /**
   * Récupère la liste des bases de données liées à la news
   *
   * @param $uids
   * @param bool $digest
   * @return array
   */
  private function getRelatedDatabases($uids, $digest = false) {
    $array = [];
    if($uids) {
      foreach ($uids as $uid) {
        $database = $this->getContentElementFromDatabase($uid, 'mask_database_header');
        if($digest) {
          $data = $database['tx_mask_title'];
        } else {
          $data = new StdClass();
          $data->ref = $database['tx_mask_database_unique_key'];
          $data->value = $database['tx_mask_title'];
        }
        array_push($array,$data);
      }
    }
    return $array;
  }

  /**
  * Add tag data of the News to the index.
  *
  * @param \GeorgRinger\News\Domain\Model\News   $news
  * @param tx_mksearch_interface_IndexerDocument $indexDoc
  * @param array                                 $options
  */
  // @codingStandardsIgnoreStart (interface/abstract/unittest mistake)
  protected function getNewsTags(
    /* \GeorgRinger\News\Domain\Model\News */ $news,
    tx_mksearch_interface_IndexerDocument $indexDoc
  ) {
    // @codingStandardsIgnoreEnd
    $tags = [];
    foreach ($news->getTags() as $tag) {
      array_push($tags, $tag->getTitle());
    }
    return $tags;
  }

  /**
   * Ajoute une news dans une sélection de notice
   * @param $selectionId
   * @param $newsTitle
   * @param $permalink
   */
  private function addNewsInSelection($selectionId, $newsTitle, $permalink)
  {
    $additionalOptions = [
      'headers' => [
        'Authorization' => 'Basic ' . $this->getBase64AdminCredentials()
      ],
      'json' => [
        'newsUrl' => $permalink,
        'newsTitle' => $newsTitle,
        'selectionId' => $selectionId
      ]
    ];
    $url = $GLOBALS['AGORHA2_API_URL'] . "/contribution/news/addToSelection";
    try {
      $client = new \GuzzleHttp\Client();
      $client->request('POST', $url, $additionalOptions);
    } catch (\GuzzleHttp\Exception\ClientException $e) {/* do nothing */ }
    catch (\GuzzleHttp\Exception\GuzzleException $e) { /* do nothing */ }
  }

  /**
   * Supprime une news d'une sélection de notice
   * @param $selectionData
   * @param $permalink
   */
  private function removeNewsFromSelection($selectionData, $permalink)
  {
    $selectionId = "";
    if ($selectionData) {
      $selectionId = $selectionData->ref;
    }
    $additionalOptions = [
      'headers' => [
        'Authorization' => 'Basic ' . $this->getBase64AdminCredentials()
      ],
      'json' => [
        'selectionId' => $selectionId,
        'newsUrl' => $permalink
      ]
    ];
    $url = $GLOBALS['AGORHA2_API_URL'] . "/contribution/news/removeFromSelection";
    try {
      $client = new \GuzzleHttp\Client();
      $client->request('PUT', $url, $additionalOptions);
    } catch (\GuzzleHttp\Exception\ClientException $e) {/* do nothing */ }
    catch (\GuzzleHttp\Exception\GuzzleException $e) { /* do nothing */ }
  }

  /**
   * Cette méthode permet de mettre à jour les news si une des tables liées a été modifiée
   * En l'occurrence si un tag ou une base de données(related_pages) a été modifié
   *
   * @param string $tableName
   * @param array $sourceRecord
   * @param tx_mksearch_interface_IndexerDocument $indexDoc
   * @param array $options
   *
   * @return bool
   */
  protected function stopIndexing(
    $tableName,
    $sourceRecord,
    tx_mksearch_interface_IndexerDocument $indexDoc,
    $options
  ) {
    $stopIndexing = parent::stopIndexing($tableName, $sourceRecord, $indexDoc, $options);
    $relatedTablesMapping = [
      [
        'main' => 'tx_news_domain_model_tag',
        'mm' => ['tx_news_domain_model_news_tag_mm']
      ]
    ];
    if($this->checkIfDataIsRelatedAndUpdate($tableName, $sourceRecord['uid'], $relatedTablesMapping)){
      $stopIndexing = true;
    }
    return $stopIndexing;
  }

  /**
   * Connection Basic pour api news/notices
   * @return string
   */
  private function getBase64AdminCredentials() {
    return base64_encode($GLOBALS['USER_ADMIN_MAIL'].':'.$GLOBALS['USER_ADMIN_PASSWORD']);
  }
}

if (defined('TYPO3_MODE') && $GLOBALS['TYPO3_CONF_VARS'][TYPO3_MODE]['XCLASS']['ext/mksearch/indexer/class.tx_mksearch_indexer_AgorhaNews.php']) {
  include_once $GLOBALS['TYPO3_CONF_VARS'][TYPO3_MODE]['XCLASS']['ext/mksearch/indexer/class.tx_mksearch_indexer_AgorhaNews.php'];
}
