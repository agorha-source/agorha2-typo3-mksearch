<?php

use DMK\Mksearch\Utility\PiazzaRepositoryUtility;

/**
 * Class tx_mksearch_indexer_PiazzaEvenement
 * Service d'indexation appelé par l'extension "mksearch".
 * @author Sword SSl
 */
class tx_mksearch_indexer_PiazzaEvenement extends tx_mksearch_indexer_BasePiazza {
    const STRING_UID_PREFIX = 'cms_event_';

    /** @var \Sword\PiazzaAgenda\Domain\Repository\EvenementRepository $repository*/
    protected $repository = null;

    /**
     * Retourne l'identification du type de contenu.
     * Cette identification fait partie des données indexées et est utilisée lors de recherches ultérieures
     * pour identifier les résultats de la recherche. Vous êtes complètement libre dans la plage de valeurs,
     * mais faites attention car vous êtes en même temps responsable de l'unicité (c'est-à-dire pas de
     * chevauchement avec d'autres types de contenu) et de la cohérence (c'est-à-dire la reconnaissance)
     * sur l'indexation et la recherche de données.
     *
     * @return array
     */
    public static function getContentType()
    {
        return array('piazza_agenda', 'evenement');
    }

    /**
     * @const TYPE_EVENEMENT_BY_FIELD array
     * Selon le type d'evenement, differents champs sont indexables
     * Représentation sous forme de tableau du document:
     * \Clients\SSL\Clients\Centre Pompidou\Piazza\Product\Concept\CON00110 - Conception CMS\Modele_Evenement.xlsx
     * Pour chaque champ, on déclare l'UID du(des) type(s) d'évènement dans le(s)quel(s) il est utilisé
     * 0:public, 1:privé, 2:technique
     */
    const TYPE_EVENEMENT_BY_FIELD = [
        'hidden' => [0, 1, 2],
        'type_evenement' => [0, 1, 2],
        'statut_editorial' => [0],
        'a_confirmer' => [0, 1, 2],
        'annule' => [0, 1, 2],
        'titre_principal' => [0,1,2],
        'sous_titre' => [0],
        'initiative_orga_be' => [0, 1, 2],
        'initiative_orga_fe' => [0, 1, 2],
        'categories_publiques' => [0],
        'categories_privees' => [1],
        'categories_techniques' => [2],
        'types_publics' => [0],
        'types_accessibilites' => [0],
        'tarifs' => [0, 1, 2],
        'lien_billeterie' => [0],
        'precisions' => [1, 2],
        'informations_speciales' => [0, 1, 2],
        'lieux_internes' => [0, 1, 2],
        'lieux_externes' => [0],
        'lieux_receptions' => [1, 2],
        'horaires' => [0, 1, 2],
        'date_affichage' => [0],
        'date_information' => [0],
        'complet' => [0, 1, 2],
        'description_courte' => [0],
        'description_longue' => [0],
        'texte_additionnel' => [0],
        'intervenant' => [0],
        'oeuvres_liees' => [0],
        'liens_externes' => [0],
        'mots_cles' => [0],
        'lien_facebook' => [0],
        'lien_twitter' => [0],
        'lien_instagram' => [0],
        'lien_youtube' => [0],
        'lien_soundcloud' => [0],
        'lien_deezer' => [0],
        'lien_pinterest' => [0],
        'parent' => [0],
        'programmation_associee' => [0],
        'programmation_associee_sr' => [0],
        'vous_aimerez_aussi' => [0],
        'vous_aimerez_aussi_sr' => [0],
        'mecenes' => [0],
        'partenaires' => [0],
        'partenaires_medias' => [0],
        'visuel_portrait' => [0],
        'visuel_paysage' => [0],
        'visuel_panoramique' => [0],
        'bande_annonce' => [0],
        'podcast' => [0],
        'media' => [0],
        'index_depublier' => [0],
        'index_forcer' => [0],
        'index_bientot' => [0],
        'index_derniers_jours' => [0],
        'index_tri_agenda' => [0],
        'index_tri_recherche' => [0],
    ];

    protected function isFieldUsedByTypeEvenement($field, $type)
    {
        if (!array_key_exists($field, self::TYPE_EVENEMENT_BY_FIELD)) {
            return false;
        }
        if (in_array($type, self::TYPE_EVENEMENT_BY_FIELD[$field])) {
            return true;
        }
        return false;
    }

    /**
     * (non-PHPdoc).
     *
     * @see tx_mksearch_interface_Indexer::prepareSearchData()
     */
    public function prepareSearchData($tableName, $rawData, tx_mksearch_interface_IndexerDocument $indexDoc, $options)
    {
        if (
            'tx_piazzaagenda_domain_model_evenement' !== $tableName
            && 'tx_piazzaagenda_domain_model_categorieprivee' !== $tableName
            && 'tx_piazzaagenda_domain_model_categoriepublique' !== $tableName
            && 'tx_piazzaagenda_domain_model_categorietechnique' !== $tableName
            && 'tx_piazzaagenda_domain_model_lieuexterne' !== $tableName
            && 'tx_piazzaagenda_domain_model_tarif' !== $tableName
            && 'tx_piazzaagenda_domain_model_typeaccessibilite' !== $tableName
            && 'tx_piazzaagenda_domain_model_typepublic' !== $tableName
            && 'tx_piazzamotscles_domain_model_motcle' !== $tableName
            && 'tx_piazzalieux_domain_model_lieu' !== $tableName
        ) {
            if (tx_rnbase_util_Logger::isWarningEnabled()) {
                tx_rnbase_util_Logger::warn(__METHOD__ . ': Unknown table "' . $tableName . '" given.', 'mksearch', array('tableName' => $tableName));
            }
            return null;
        }
        if (!$this->isIndexableRecord($rawData, $options)) {
            return null; //no need to index
        }
        if ($this->stopIndexing($tableName, $rawData, $indexDoc, $options)) {
            return null;
        }

        $objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager');
        /* @var \Sword\PiazzaAgenda\Domain\Repository\EvenementRepository  $this->repository */
        $this->repository = $objectManager->get(\Sword\PiazzaAgenda\Domain\Repository\EvenementRepository::class);
        $record = $this->repository->findAllRecordsByUid($rawData['uid'], $rawData['pid']);
        if (!$record instanceof \Sword\PiazzaAgenda\Domain\Model\Evenement) {
            throw new Exception("Evenement introuvable. Uid: {$rawData['uid']}", 404);
        }

        /*
         * Indexation des champs
         */
        $indexDoc->setUid(self::STRING_UID_PREFIX . $rawData['uid']);
        $indexDoc->addField('id', self::STRING_UID_PREFIX . $rawData['uid']);

        $indexDoc->addField('l10n_parent', $rawData['l10n_parent']);
        $indexDoc->addField('sys_language_uid', $rawData['sys_language_uid']);
        $indexDoc->setTimestamp($rawData['tstamp']);

        $indexDoc->addField('etl_processed', false);

        if ($this->hasDocToBeDeleted($rawData, $options)) {
            $indexDoc->addField('deleted', true);
            return $indexDoc;
        }
        else {
            $indexDoc->addField('deleted', false);
        }

        $indexDoc->addField('hidden', ($rawData['hidden']) ? (bool)$rawData['hidden'] : false);
        $indexDoc->addField('starttime', $rawData['starttime']);
        $indexDoc->addField('endtime', $rawData['endtime']);
        $indexDoc->addField('type_evenement', $this->getProperty($record, 'type_evenement'));

        $indexDoc->addField('statut_editorial', $this->getProperty($record, 'statut_editorial'));
        $indexDoc->addField('a_confirmer', $this->getProperty($record, 'a_confirmer'));
        $indexDoc->addField('annule', $this->getProperty($record, 'annule'));
        $indexDoc->addField('titre_principal', $this->getProperty($record, 'titre_principal'));
        $indexDoc->addField('sous_titre', $this->getProperty($record, 'sous_titre'));
        $indexDoc->addField('initiative_orga_be', $this->getProperty($record, 'initiative_orga_be'));
        $indexDoc->addField('initiative_orga_fe', $this->getProperty($record, 'initiative_orga_fe'));
        $indexDoc->addField('categories_publiques', $this->getProperty($record, 'categories_publiques', PiazzaRepositoryUtility::STRING_H_CONCATENATED));
        $indexDoc->addField('categories_publiques_first_level', $this->getProperty($record, 'categories_publiques', PiazzaRepositoryUtility::STRING_H_FIRST_LEVEL));
        $indexDoc->addField('categories_publiques_last_level', $this->getProperty($record, 'categories_publiques', PiazzaRepositoryUtility::STRING_H_LAST_LEVEL));
        $indexDoc->addField('categories_privees', $this->getProperty($record, 'categories_privees', PiazzaRepositoryUtility::STRING_H_CONCATENATED));
        $indexDoc->addField('categories_privees_first_level', $this->getProperty($record, 'categories_privees', PiazzaRepositoryUtility::STRING_H_FIRST_LEVEL));
        $indexDoc->addField('categories_privees_last_level', $this->getProperty($record, 'categories_privees', PiazzaRepositoryUtility::STRING_H_LAST_LEVEL));
        $indexDoc->addField('categories_techniques', $this->getProperty($record, 'categories_techniques', PiazzaRepositoryUtility::STRING_H_CONCATENATED));
        $indexDoc->addField('categories_techniques_first_level', $this->getProperty($record, 'categories_techniques', PiazzaRepositoryUtility::STRING_H_FIRST_LEVEL));
        $indexDoc->addField('categories_techniques_last_level', $this->getProperty($record, 'categories_techniques', PiazzaRepositoryUtility::STRING_H_LAST_LEVEL));
        $indexDoc->addField('types_publics', $this->getProperty($record, 'types_publics'));
        $indexDoc->addField('types_accessibilites', $this->getProperty($record, 'types_accessibilites'));
        $indexDoc->addField('tarifs', $this->getProperty($record, 'tarifs'));
        $indexDoc->addField('lien_billeterie', $this->getProperty($record, 'lien_billeterie'));
        $indexDoc->addField('precisions', $this->getProperty($record, 'precisions'));
        $indexDoc->addField('informations_speciales', $this->getProperty($record, 'informations_speciales'));
        $indexDoc->addField('lieux_internes', $this->getProperty($record, 'lieux_internes', PiazzaRepositoryUtility::STRING_H_PLACES));
        $indexDoc->addField('lieux_receptions', $this->getProperty($record, 'lieux_receptions', PiazzaRepositoryUtility::STRING_H_PLACES));
        $indexDoc->addField('lieux_externes', $this->getProperty($record, 'lieux_externes', PiazzaRepositoryUtility::STRING_H_PLACES));
        $indexDoc->addField('horaires', $this->getProperty($record, 'horaires'));
        $indexDoc->addField('date_affichage', $this->getProperty($record, 'date_affichage'));
        $indexDoc->addField('date_information', $this->getProperty($record, 'date_information'));
        $indexDoc->addField('complet', $this->getProperty($record, 'complet'));
        $indexDoc->addField('description_courte', $this->getProperty($record, 'description_courte'));
        $indexDoc->addField('description_longue', $this->getProperty($record, 'description_longue'));
        $indexDoc->addField('texte_additionnel', $this->getProperty($record, 'texte_additionnel'));
        $indexDoc->addField('informations_speciales', $this->getProperty($record, 'informations_speciales'));
        $indexDoc->addField('intervenant', $this->getProperty($record, 'intervenant'));
        //$indexDoc->addField('oeuvres_liees', $this->getProperty($record,'oeuvres_liees'));
        $indexDoc->addField('liens_externes', $this->getProperty($record, 'liens_externes'));
        $indexDoc->addField('mots_cles', $this->getProperty($record, 'mots_cles'));
        $indexDoc->addField('lien_facebook', $this->getProperty($record, 'lien_facebook'));
        $indexDoc->addField('lien_twitter', $this->getProperty($record, 'lien_twitter'));
        $indexDoc->addField('lien_instagram', $this->getProperty($record, 'lien_instagram'));
        $indexDoc->addField('lien_youtube', $this->getProperty($record, 'lien_youtube'));
        $indexDoc->addField('lien_soundcloud', $this->getProperty($record, 'lien_soundcloud'));
        $indexDoc->addField('lien_deezer', $this->getProperty($record, 'lien_deezer'));
        $indexDoc->addField('lien_pinterest', $this->getProperty($record, 'lien_pinterest'));
        $indexDoc->addField('parent', $this->getProperty($record, 'parent', PiazzaRepositoryUtility::STRING_PARENT_RELATION, ['uid_prefix' => self::STRING_UID_PREFIX]));
        $indexDoc->addField('programmation_associee', $this->getProperty($record, 'programmation_associee', PiazzaRepositoryUtility::STRING_PARENT_RELATION, ['uid_prefix' => self::STRING_UID_PREFIX]));
        $indexDoc->addField('programmation_associee_sr', $this->getProperty($record, 'programmation_associee_sr', PiazzaRepositoryUtility::STRING_PARENT_RELATION, ['uid_prefix' => self::STRING_UID_PREFIX]));
        $indexDoc->addField('vous_aimerez_aussi', $this->getProperty($record, 'vous_aimerez_aussi', PiazzaRepositoryUtility::STRING_PARENT_RELATION, ['uid_prefix' => self::STRING_UID_PREFIX]));
        $indexDoc->addField('vous_aimerez_aussi_sr', $this->getProperty($record, 'vous_aimerez_aussi_sr', PiazzaRepositoryUtility::STRING_PARENT_RELATION, ['uid_prefix' => self::STRING_UID_PREFIX]));
        $indexDoc->addField('mecenes', $this->getProperty($record, 'mecenes'));
        $indexDoc->addField('partenaires', $this->getProperty($record, 'partenaires'));
        $indexDoc->addField('partenaires_medias', $this->getProperty($record, 'partenaires_medias'));
        $indexDoc->addField('visuel_portrait', $this->getProperty($record, 'visuel_portrait'));
        $indexDoc->addField('visuel_paysage', $this->getProperty($record, 'visuel_paysage'));
        $indexDoc->addField('visuel_panoramique', $this->getProperty($record, 'visuel_panoramique'));
        $indexDoc->addField('bande_annonce', $this->getProperty($record, 'bande_annonce'));
        $indexDoc->addField('podcast', $this->getProperty($record, 'podcast'));
        $indexDoc->addField('media', $this->getProperty($record, 'media'));
        $indexDoc->addField('index_depublier', $this->getProperty($record, 'index_depublier'));
        $indexDoc->addField('index_forcer', $this->getProperty($record, 'index_forcer'));
        $indexDoc->addField('index_bientot', $this->getProperty($record, 'index_bientot'));
        $indexDoc->addField('index_derniers_jours', $this->getProperty($record, 'index_derniers_jours'));
        $indexDoc->addField('index_tri_agenda', $this->getProperty($record, 'index_tri_agenda'));
        $indexDoc->addField('index_tri_recherche', $this->getProperty($record, 'index_tri_recherche'));

        $indexDoc->addField('type_fiche', $this->getTypeFiche($record));

        return $indexDoc;
    }

    /**
     * @param \Sword\PiazzaAgenda\Domain\Model\Evenement $record
     * @param $property
     * @param string|null $specialGetter
     * @param array $specialGetterParameters
     * @return array|stdClass|null
     */
    protected function getProperty($record, $property, $specialGetter = null, $specialGetterParameters = [])
    {
        // les valeurs non gérées par le type passent à NULL (permet de désindexer les champs inutilisés en cas de changement de type)
        if (!$this->isFieldUsedByTypeEvenement($property, $record->getTypeEvenement())) {
            return null;
        }
        return parent::getProperty(
            $record,
            $property,
            $specialGetter,
            $specialGetterParameters
        );
    }

    /**
     * @param \Sword\PiazzaAgenda\Domain\Model\Horaire $property
     * @return stdClass|null
     */
    protected function getHoraires($property)
    {
        if (!$property->getDateDebut()) { // (requiered field)
            return null;
        }
        $horaires = new stdClass();
        $horaires->date_debut = ($property->getDateDebut() instanceof DateTime) ? gmdate('Y-m-d', $property->getDateDebut()->getTimestamp()) : null;
        $horaires->date_fin = ($property->getDateFin() instanceof DateTime) ? gmdate('Y-m-d', $property->getDateFin()->getTimestamp()) : null;
        $horaires->heure_debut = ($property->getHeureDebut()) ? gmdate('H:i:s', $property->getHeureDebut()) : null;
        $horaires->heure_fin = ($property->getHeureFin()) ? gmdate('H:i:s', $property->getHeureFin()) : null;
        $horaires->jour_semaine = ($property->getJourSemaine()) ? $property->getJourSemaine() : null;
        return $horaires;
    }

    /**
     * @param \Sword\PiazzaAgenda\Domain\Model\Evenement $property
     * @return int
     */
    protected function getEvenements($property)
    {
        return $property->getUid();
    }

    /**
     * @param int $property
     * @return string
     */
    protected function getTypeEvenement($property)
    {
        switch ($property->getTypeEvenement()) {
            case 1:
                $return = 'private';
                break;
            case 2:
                $return = 'technical';
                break;
            case 0:
            default:
                $return = 'public';
        }
        return $return;
    }

    /**
     * @param $property
     * @return stdClass
     */
    protected function getMecenes($property)
    {
        return $this->getMecenesPartenaires($property);
    }

    /**
     * @param $property
     * @return stdClass
     */
    protected function getPartenaires($property)
    {
        return $this->getMecenesPartenaires($property);
    }

    /**
     * @param $property
     * @return stdClass
     */
    protected function getPartenairesMedias($property)
    {
        return $this->getMecenesPartenaires($property);
    }

    /**
     * @param \Sword\PiazzaAgenda\Domain\Model\Mecene|\Sword\PiazzaAgenda\Domain\Model\Partenaire|\Sword\PiazzaAgenda\Domain\Model\PartenaireMedia $property
     * @return stdClass
     */
    protected function getMecenesPartenaires($property)
    {
        $obj = new \stdClass();
        $obj->description = $property->getDescription();
        if ($property->getImage()) {
            $obj->image_uid = $property->getImage()->getOriginalResource()->toArray()['uid_local'];
            $obj->image_path = $property->getImage()->getOriginalResource()->toArray()['url'];
        }
        return $obj;
    }

    /**
     * @param \Sword\PiazzaAgenda\Domain\Model\TypePublic $property
     * @return string
     */
    protected function getTypesPublics($property)
    {
        return $property->getLibelle();
    }

    /**
     * @param \Sword\PiazzaAgenda\Domain\Model\TypeAccessibilite $property
     * @return string
     */
    protected function getTypesAccessibilites($property)
    {
        return $property->getLibelle();
    }

    /**
     * @param \Sword\PiazzaAgenda\Domain\Model\Tarif $property
     * @return string
     */
    protected function getTarifs($property)
    {
        return $property->getLibelle();
    }

    /**
     * @param \Sword\PiazzaAgenda\Domain\Model\LienExterne $property
     * @return stdClass
     */
    protected function getLiensExternes($property)
    {
        $return = new \stdClass();
        $return->libelle = $property->getLibelle();
        $return->url = $property->getUrl();
        return $return;
    }

    /**
     * @param \Sword\PiazzaMotscles\Domain\Model\Motcle $property
     * @return string
     */
    protected function getMotsCles($property)
    {
        return $property->getLibelle();
    }

    /**
     * "CYCLE" l'événement a des enfants qui lui font référence
     * "EVENEMENT_ISOLE" l'événement n'a pas d'enfants et  n'a pas de parent
     *  "SEANCE" l'événement a un parent
     * Attention, si l'événement est parent (a des enfants) mais a aussi un parent : Privilégier "CYCLE"
     *
     * @param \Sword\PiazzaAgenda\Domain\Model\Evenement $record
     * @return string
     */
    protected function getTypeFiche($record)
    {
        if($record->getTypeEvenement() !== 0) {
            return null;
        }
        $hasParent = false;
        $hasChildren = false;
        // check parent
        if ($record->getParent()->toArray()[0]) {
            $hasParent = true;
        }
        // check children
        $children = $this->repository->countChildren($record->getUid());
        if ($children > 0) {
            $hasChildren = true;
        }

        $type = null;

        if ($hasParent && $hasChildren) {
            $type = "CYCLE";
        }
        else if (!$hasParent && $hasChildren) {
            $type = "CYCLE";
        }
        else if ($hasParent && !$hasChildren) {
            $type = "SEANCE";
        }
        else if (!$hasParent && !$hasChildren) {
            $type = "EVENEMENT_ISOLE";
        }
        return $type;
    }
    
    
    /**
     * Shall we break the indexing for the current data?
     *
     * when an indexer is configured for more than one table
     * the index process may be different for the tables.
     * overwrite this method in your child class to stop processing and
     * do something different like putting a record into the queue
     * if it's not the table that should be indexed
     *
     * @param string $tableName
     * @param array $sourceRecord
     * @param tx_mksearch_interface_IndexerDocument $indexDoc
     * @param array $options
     *
     * @return bool
     */
    protected function stopIndexing(
        $tableName,
        $sourceRecord,
        tx_mksearch_interface_IndexerDocument $indexDoc,
        $options
    ) {
        $stopIndexing = parent::stopIndexing(
            $tableName,
            $sourceRecord,
            $indexDoc,
            $options
        );

        $relatedTablesMapping = [
            [
                'main' => 'tx_piazzaagenda_domain_model_categorieprivee',
                'mm' => ['tx_piazzaagenda_evenement_categorieprivee_mm'],
                'hierarchicalField' => 'parent'
            ],
            [
                'main' => 'tx_piazzaagenda_domain_model_categoriepublique',
                'mm' => ['tx_piazzaagenda_evenement_categoriepublique_mm'],
                'hierarchicalField' => 'parent'
            ],
            [
                'main' => 'tx_piazzaagenda_domain_model_categorietechnique',
                'mm' => ['tx_piazzaagenda_evenement_categorietechnique_mm'],
                'hierarchicalField' => 'parent'
            ],
            [
                'main' => 'tx_piazzaagenda_domain_model_typeaccessibilite',
                'mm' => ['tx_piazzaagenda_evenement_typeaccessibilite_mm'],
                'hierarchicalField' => false
            ],
            [
                'main' => 'tx_piazzaagenda_domain_model_typepublic',
                'mm' => ['tx_piazzaagenda_evenement_typepublic_mm'],
                'hierarchicalField' => false
            ],
            [
                'main' => 'tx_piazzaagenda_domain_model_tarif',
                'mm' => ['tx_piazzaagenda_evenement_tarif_mm'],
                'hierarchicalField' => false
            ],
            [
                'main' => 'tx_piazzamotscles_domain_model_motcle',
                'mm' => ['tx_piazzaagenda_evenement_motcle_mm'],
                'hierarchicalField' => false
            ],
            [
                'main' => 'tx_piazzaagenda_domain_model_lieuexterne',
                'mm' => ['tx_piazzaagenda_evenement_lieuexterne_mm'],
                'hierarchicalField' => 'parent'
            ],
            [
                'main' => 'tx_piazzalieux_domain_model_lieu',
                'mm' => ['tx_piazzaagenda_evenement_lieu_mm', 'tx_piazzaagenda_evenement_lieuxreceptions_lieu_mm'],
                'hierarchicalField' => 'parent'
            ],
        ];

        if($this->checkRelatedData($tableName, $sourceRecord['uid'], $relatedTablesMapping)){
            $stopIndexing = true;
        }

        return $stopIndexing;
    }

}

if (defined('TYPO3_MODE') && $GLOBALS['TYPO3_CONF_VARS'][TYPO3_MODE]['XCLASS']['ext/mksearch/indexer/class.tx_mksearch_indexer_PiazzaEvenement.php']) {
    include_once $GLOBALS['TYPO3_CONF_VARS'][TYPO3_MODE]['XCLASS']['ext/mksearch/indexer/class.tx_mksearch_indexer_PiazzaEvenement.php'];
}

