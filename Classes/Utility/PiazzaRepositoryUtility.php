<?php

namespace DMK\Mksearch\Utility;

/**
 * Class RepositoryUtility
 * @package DMK\Mksearch\Utility
 * @author Sword SSL
 */
final class PiazzaRepositoryUtility {
    const DEFAULT_SEPARATOR = " # ";

    public const STRING_H_CONCATENATED = "hierarchical_concatenated";
    public const STRING_H_FIRST_LEVEL = "hierarchical_first_level";
    public const STRING_H_LAST_LEVEL = "hierarchical_last_level";
    public const STRING_H_PLACES = "hierarchical_places";
    public const STRING_PARENT_RELATION = "parent";
    public const STRING_LAZY_FILE = "lazyloaded_file";

    /**
     * @param $object
     * @param null $specialGetter
     * @param array $specialGetterParameters
     * @return \stdClass|string|null
     */
    public static function getObject($object, $specialGetter = null, $specialGetterParameters = [])
    {
        switch ($specialGetter) {
            case self::STRING_H_FIRST_LEVEL:
                $item = self::getHierarchicalFirstLevel($object);
                break;
            case self::STRING_H_LAST_LEVEL:
                $item = self::getHierarchicalLastLevel($object);
                break;
            case self::STRING_H_CONCATENATED:
                $item = self::getHierarchicalConcatenatedLabels($object);
                break;
            case self::STRING_H_PLACES:
                $item = self::getHierarchicalPlaces($object);
                break;
            case self::STRING_PARENT_RELATION:
                $item = self::getParentRelationUid($object, $specialGetterParameters['uid_prefix']);
                break;
            case self::STRING_LAZY_FILE:
                $item = self::lazyLoadedFile($object, $specialGetterParameters['tablenames'], $specialGetterParameters['fieldname']);
                break;
            default:
                $item = null;
                break;
        }
        return $item;
    }

    /**
     * @param $property
     * @param string $uidPrefix
     * @return string
     */
    private static function getParentRelationUid($property, $uidPrefix = '')
    {
        $prefix = ($uidPrefix !== '') ? $uidPrefix : '';
        return $prefix . $property->getUid();
    }

    /**
     * @param $property
     * @param $str
     * @return string
     */
    private static function getHierarchicalConcatenatedLabels($property, &$str = '')
    {
        $str = $property->getLibelle() . $str;
        $propertyType = get_class($property);
        if ($property->getParent() instanceof $propertyType) {
            $str = self::DEFAULT_SEPARATOR . $str;
            self::getHierarchicalConcatenatedLabels($property->getParent(), $str);
        }
        return $str;
    }

    /**
     * @param $property
     * @param array $array
     * @return string
     */
    private static function getHierarchicalFirstLevel($property, &$array = [])
    {
        array_unshift($array, $property->getLibelle());
        $propertyType = get_class($property);
        if ($property->getParent() instanceof $propertyType) {
            self::getHierarchicalFirstLevel($property->getParent(), $array);
        }
        return $array[0];
    }

    /**
     * @param $property
     * @param array $array
     * @return string
     */
    private static function getHierarchicalLastLevel($property, &$array = [])
    {
        array_unshift($array, $property->getLibelle());
        $propertyType = get_class($property);
        if ($property->getParent() instanceof $propertyType) {
            self::getHierarchicalLastLevel($property->getParent(), $array);
        }
        return end($array);
    }

    /**
     * @param $property
     * @param array $array
     * @return \stdClass
     */
    private static function getHierarchicalPlaces($property, &$array = [])
    {
        array_unshift($array, $property->getLibelle());
        $propertyType = get_class($property);
        if ($property->getParent() instanceof $propertyType) {
            self::getHierarchicalPlaces($property->getParent(), $array);
        }
        return self::arborescencePiazzaLieux($array);
    }

    /**
     * @param $array
     * @return \stdClass
     */
    private static function arborescencePiazzaLieux($array)
    {
        $obj = new \stdClass();
        foreach ($array as $level => $value) {
            if ($level === 0) {
                $obj->pays = $value;
            }
            if ($level === 1) {
                $obj->ville = $value;
            }
            if ($level === 2) {
                $obj->batiment = $value;
            }
            if ($level === 3) {
                $obj->salle = $value;
            }
        }
        return $obj;
    }

    /**
     * @param $property
     * @return \stdClass
     */
    public static function getFileReferenceAsObject($property)
    {
        $fileInfos = $property->getOriginalResource()->toArray();
        return self::arrayToPiazzaFileObject($fileInfos);
    }

    /**
     * @param $array
     * @return \stdClass
     */
    private static function arrayToPiazzaFileObject($array)
    {
        $return = new \stdClass();
        $return->uid = $array['uid_local'];
        $return->path = $array['url'];
        return $return;
    }

    /**
     * @param $string
     * @param bool $lowerFirst
     * @return string
     */
    public static function underscoresToCamelCase($string, $lowerFirst = false)
    {
        $str = str_replace('_', '', ucwords($string, '_'));
        if ($lowerFirst) {
            $str = lcfirst($str);
        }
        return $str;
    }

    /**
     * @param $record
     * @param $tableName
     * @param $fieldName
     * @return \stdClass|null
     */
    public static function lazyLoadedFile($record, $tableName, $fieldName)
    {
        if (is_array($record)) {
            if (!$record[$fieldName]) {
                return null;
            }
            else {
                $uid = $record['uid'];
            }
        }
        $queryBuilder = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Database\ConnectionPool::class)
            ->getQueryBuilderForTable('sys_file');
        $statement = $queryBuilder
            ->select('sys_file.uid', 'sys_file.identifier')
            ->from('sys_file')
            ->join(
                'sys_file',
                'sys_file_reference',
                'reference',
                $queryBuilder->expr()->eq('reference.uid_local', $queryBuilder->quoteIdentifier('sys_file.uid'))
            )
            ->where(
                $queryBuilder->expr()->eq('reference.uid_foreign', $queryBuilder->createNamedParameter($uid, \PDO::PARAM_INT)),
                $queryBuilder->expr()->eq('reference.tablenames', $queryBuilder->createNamedParameter($tableName, \PDO::PARAM_STR)),
                $queryBuilder->expr()->eq('reference.fieldname', $queryBuilder->createNamedParameter($fieldName, \PDO::PARAM_STR))
            )
            ->execute();
        $result = $statement->fetch();
        return self::arrayToPiazzaFileObject($result);
    }


//    /**
//     * Dynamic getter on property type
//     * @param $property
//     * @return mixed
//     */
//    private static function dispatchRelationGetter($property)
//    {
//        $relationTypeName = end(explode("\\", get_class($property)));
//        $relationGetter = 'get' . $relationTypeName;
//        if (!method_exists(__CLASS__, $relationGetter)) {
//            debug("Type ".get_class($property)." non géré, '".__CLASS__."::{$relationGetter}()' introuvable");
//            return null;
//        }
//        return self::{$relationGetter}($property);
//    }

}
